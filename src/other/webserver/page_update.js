// This code is part of Revelation.
// Copyright (C) 2017-2023 Jared Henley.

var current_id = 0;
var last_update = Date.now();
var theme_url = '';
var slide_def;
var transition_time = 100; // milliseconds
var html;
var slide_class;
var do_transition = document.location.pathname.includes("projector");

function start_updates() {
    // Check for changes to the page content & check that it's working too
    if (do_transition)
        init();
    setTimeout(load_changes, 0);
    setTimeout(detect_failure, 5000);
}

function init() {
    // Set up the transition
    container = document.getElementById("container");
    container.style.transitionProperty = "opacity";
    container.style.transitionDuration = transition_time + "ms";
    container.style.transitionTimingFunction = "ease-in-out";
    container.ontransitionend = change_content;
}

function content_loaded() {
    // Called when page change data arrives from the server (over XMLHTTP)
    last_update = Date.now();
    html = this.responseText;
    // Query the DOM
    background = document.getElementById("background");
    container = document.getElementById("container");
    content = document.getElementById("content");
    link_theme = document.getElementById("theme");
    // Update the theme if required
    current_theme_url = "/theme" + slide_def.theme + "/" + template + ".css";
    if (theme_url != current_theme_url) {
        theme_url = current_theme_url;
        link_theme.href = current_theme_url;
    }
    if (template == "projector")
        slide_class = slide_def.projector_class;
    else
        slide_class = slide_def.stage_class;
    if (content_different()) {
        // Change the slide content - using a transition if necessary
        container.style.opacity = 0; // Opacity change triggers transition
        if (!do_transition)
            change_content();
    }
}

function content_different() {
    return content.innerHTML !== html;
}

function change_content() {
    // Actually change the slide content
    // Don't do this when transitioning back to opaque
    if (container.style.opacity == 0) {
        container.className = slide_class;
        content.innerHTML = html;
        images = content.getElementsByTagName("img");
        if (images.length > 0) {
            images[0].onload = show_content;
        } else
            show_content();
    }
}

function show_content() {
    // Make the content opaque again
    container.style.opacity = 1;
    // Check for changes
    queue_reload();
}

function slide_def_loaded() {
    // Update the time that the code last ran
    last_update = Date.now();
    // Get some basic variables about the slide - this includes everything
    // outside the slide content, so that we don't hit up the server extra
    // times for tiny amounts of info.
    slide_def = JSON.parse(this.responseText);
    // Get the id for this template from the server response
    if (template == "overview")
        server_id = slide_def.overview_id;
    else
        server_id = slide_def.slide_id;
    if (template == "stage") {
        background = document.getElementById("background");
        background.className = slide_def.timer_class;
        timer_element = document.getElementById("timer");
        clock_element = document.getElementById("clock");
        elapsed_element = document.getElementById("elapsed");
        timer_element.innerHTML = slide_def.timer_string;
        clock_element.innerHTML = slide_def.clock_string;
        elapsed_element.innerHTML = slide_def.elapsed_string;
    }
    if (server_id != current_id) {
        // If the id has changed, grab the rest of the slide content
        // The id will be checked later as part of that process
        current_id = server_id;
        content_rq = new XMLHttpRequest();
        content_rq.onload = content_loaded;
        content_rq.open('GET', 'content.html', true);
        content_rq.send(null);  // No data needs to be sent along with the request.
    } else
        // Check the id again soon
        queue_reload();
}

function queue_reload() {
    if (template == "overview")
        setTimeout(load_changes, 2000);
    else
        setTimeout(load_changes, 25);
}

function load_changes() {
    var var_rq = new XMLHttpRequest();
    var_rq.onload = slide_def_loaded;
    var_rq.open('GET', '/slide_def.json', true);
    var_rq.send(null);  // No data needs to be sent along with the request.
}

function detect_failure() {
    // Every 5 seconds check if responses have been received from the server
    // within the last 10 seconds.  If not, restart because something has broken.
    if (Date.now() - last_update > 10000) {
        load_changes();
    }
    setTimeout(detect_failure, 5000);
}
