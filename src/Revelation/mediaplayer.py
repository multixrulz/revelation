#!/usr/bin/python
"""Manage the playing of audio and video files."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import subprocess
import time
import threading
import tempfile
import socket
import os

class RevMediaPlayer():
    """A wrapper around mpv."""
    def __init__(self, win_id_callback, playback_started_callback,
        playback_ended_callback, *args, **kwargs):
        """Initialise the player.

        win_id_callback: a function to call to get a Window ID to put the video output into.
        playback_started_callback: the function to call when playback starts.
        playback_ended_callback: the function to call when playback ends.
        """
        super().__init__(*args, **kwargs)
        self._paused = False
        self._playing = False
        self._reached_end = False
        self._win_id_callback = win_id_callback
        self._playback_ended_callback = playback_ended_callback
        self._playback_started_callback = playback_started_callback
        self._mpv = None

    def paused(self):
        """Return true if the media is paused.
        """
        return self._paused

    def playing(self):
        """Return true if the media is playing.
        """
        return self._playing

    def reached_end(self):
        """Returns true if the media ended (as opposed to being stopped).
        """
        return self._reached_end

    def play(self, media_slide):
        """Play a media file.

        media_slide: a RevSlide containing media.
        """
        # Default mpv arguments:
        #  --osd-level: Turn off the on-screen display
        #  --no-terminal: Don't interact with a terminal
        #  --wid: Send the output to a window ID
        #  --no-fs: Don't go fullscreen
        #  --no-border: no window borders
        #  --input-ipc-server: Take JSON commands from a unix socket
        # Optional mpv arguments
        #  --start: Specify start position (seconds, or hh:mm:ss.ms)
        #  --end: Specify end position (seconds, or hh:mm:ss.ms)
        #  --volume: Specify playback volume
        #  --loop-file: Repeat the media (N|inf|no - N times, infinite, or not at all)

        # Only allow one instance of mpv.
        self.stop()
        self._media_slide = media_slide
        ipc_socket_file='/tmp/revelation' + str(os.getpid())
        if os.path.exists(ipc_socket_file):
            os.remove(ipc_socket_file)
        mpv_command = ['mpv',
            '--osd-level=0',
            '--no-terminal',
            '--wid={}'.format(str(self._win_id_callback())),
            '--no-fs',
            '--no-border',
            '--input-ipc-server={}'.format(ipc_socket_file)]
        if media_slide.media_start() is not None:
            mpv_command.extend(
                ['--start={}'.format(media_slide.media_start())])
        if media_slide.media_end() is not None:
            mpv_command.extend(
                ['--end={}'.format(media_slide.media_end())])
        if media_slide.media_volume() is not None:
            mpv_command.extend(
                ['--volume={}'.format(str(media_slide.media_volume()))])
        if media_slide.media_loop() == True:
            mpv_command.extend(
                ['--loop-file=yes'])
        mpv_command.append(media_slide.media_path())
        try:
            self._mpv = subprocess.Popen(mpv_command)
            self._paused = False
            self._playing = True
            self._reached_end = True # Will be set to False if mpv is stopped
            self._wait_thread = threading.Thread(target=self._wait_mpv)
            self._wait_thread.start()
            self._playback_started_callback()
            for i in range(500):
                if os.path.exists(ipc_socket_file):
                    break
                else:
                    time.sleep(0.01)
            else:
                print("revelation: mpv failed to start within 5 seconds.")
            self._ipc_socket = socket.socket(family=socket.AF_UNIX)
            self._ipc_socket.connect(ipc_socket_file)
        except Exception as e:
            print("revelation: mpv call failed with exception:\n", e)

    def _wait_mpv(self):
        """Wait for mpv to exit.
        """
        self._mpv.wait()
        self._playing = False
        self._paused = False
        self._playback_ended_callback(self._reached_end)

    def beginning(self):
        """Seek to the beginning of the media file.
        """
        try:
            self._send_command('{ "command": ["seek", %s, "absolute"] }\n' %
                (self._media_slide.media_start()))
        except (AttributeError, BrokenPipeError):
            pass

    def pause_play(self):
        """Pause (or restart) the media file.
        """
        if self._paused:
            command = '{ "command": ["set_property", "pause", false] }\n'
        else:
            command = '{ "command": ["set_property", "pause", true] }\n'
        try:
            self._send_command(command)
            self._paused = not self._paused
        except (AttributeError, BrokenPipeError):
            self._paused = False

    def stop(self):
        """Stop playing the media file.
        """
        if self._mpv is not None: # It's initialised to None
            self._reached_end = False
            self._mpv.kill()
            # Wait for it to end before continuing, so we don't make a
            # mess of things (especially callbacks)
            self._mpv.wait()

    def _send_command(self, command):
        """Send a command string to mpv.
        """
        self._ipc_socket.send(command.encode('utf-8', 'ignore'))
