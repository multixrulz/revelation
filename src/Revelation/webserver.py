#!/usr/bin/python
"""Manage the webserver for the stage view."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import socketserver, socket
import http.server
import threading
import mimetypes
import urllib
import json
import datetime
# Local / relative imports
from . import config
from . import build_config

class RevHTTPServer():
    """A web server for Revelation stage view and overview.
    """
    def __init__(self):
        """Initialise the web server.

        The port is always 8080.
        """
        # init has to be called before the server gets going, to initialise the handler.
        RevHTTPRequestHandler.init()
        port = 8080
        self.httpd = RevTCPServer(("", port), RevHTTPRequestHandler)
        self.httpd_thread = threading.Thread(target = self.httpd.serve_forever)
        self.httpd_thread.start()

    def update_slide_content(self, theme_url_path, projector_html, stage_html, projector_slide_class, stage_slide_class, timer_mode, timer_end_time, show_clock, show_elapsed, elapsed_start_time):
        """Change the current slide document being served.

        theme_url_path: The URL path to the current theme.
        projector_html: The html to serve for the projector content.
        stage_html: The html to serve for the stage content.
        projector_slide_class: The html class of the projector slide.
        stage_slide_class: The html class of the stage slide.
        timer_mode: The mode of the timer (timer, alarm).
        timer_end_time: The end time for the timer and alarm modes.
        show_clock: Whether or not to show the clock.
        show_elapsed: Whether or not to show the elapsed time.
        elapsed_start_time: The starting time for the elapsed timer.
        """
        RevHTTPRequestHandler.set_slide_content(theme_url_path, projector_html, stage_html, projector_slide_class, stage_slide_class, timer_mode, timer_end_time, show_clock, show_elapsed, elapsed_start_time)

    def update_overview(self, overview_html):
        """Change the overview document being served.

        overview_html: The html to serve.
        """
        RevHTTPRequestHandler.set_overview(overview_html)

    def exit(self):
        """Shut down the web server.
        """
        self.httpd.shutdown()
        self.httpd_thread.join()
        self.httpd.server_close()


class RevTCPServer(socketserver.TCPServer):
    """Customise the TCPServer to prevent the socket from staying held
    after program exit.
    """
    def server_bind(self):
        """Bind the server to a socket.
        """
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)


class RevHTTPRequestHandler(http.server.BaseHTTPRequestHandler):
    """Decide what to do with HTTP requests.
    """
    def log_message(self, *args):
        """Don't log every HTTP request to stderr, it's annoying.
        """
        pass

    def do_GET(self):
        """Look at the URL being requested and send back something
        appropriate.
        """
        # If the requested path begins with the current theme path, fetch it.
        # Otherwise return specially defined paths, and return the index page
        # for all other paths.
        request_path =  urllib.parse.unquote(self.path)
        page_method_map = {
            "/": self._index_html,
            "/index.html": self._index_html,
            "/slide_def.json": self._slide_def_json,
            "/page_update.js": self._page_update_js,
            "/favicon.png": self._favicon_png,
            "/favicon.ico": self._favicon_png,
            "/projector": self._redirect_projector,
            "/projector/": self._redirect_projector,
            "/projector/index.html": self._projector_index_html,
            "/projector/content.html": self._projector_content_html,
            "/stage": self._redirect_stage,
            "/stage/": self._redirect_stage,
            "/stage/index.html": self._stage_index_html,
            "/stage/content.html": self._stage_content_html,
            "/overview": self._redirect_overview,
            "/overview/": self._redirect_overview,
            "/overview/index.html": self._overview_index_html,
            "/overview/content.html": self._overview_content_html,
            }
        # Handle requests for theme files
        if request_path.startswith("/theme/"):
            self._theme_content(request_path)
        # Handle requests for presentation files
        elif request_path.startswith("/presentations/"):
            self._presentation_file(request_path)
        # Handle requests for non-theme content
        else:
            try:
                method = page_method_map[request_path]
            except KeyError:
                print("HTTP GET request for '{}' failed: No matching entry in page_method_map.".format(request_path))
                self._notfound()
            else:
                method()

    def _theme_content(self, request_path):
        # Discard the leading /theme/
        request_path = request_path[7:]
        if request_path.startswith("system/"):
            # Discard the leading system/
            theme_file_path = os.path.join(build_config.SYSTEM_THEME_DIR, request_path[7:])
        elif request_path.startswith("user/"):
            theme_file_path = os.path.join(config.theme_dir(), request_path[5:])
        else:
            self._notfound()
            return
        if os.path.isfile(theme_file_path):
            (mimetype, encoding) = mimetypes.guess_type(theme_file_path)
            fd = open(theme_file_path, 'rb')
            content = fd.read()
            fd.close()
            self._send_response(mimetype, encoding, content)
        else:
            self._notfound()
            return

    def _notfound(self):
        self.send_response(404)
        self.end_headers()

    def _redirect(self, target):
        self.send_response(302)
        self.send_header('Location', target)
        self.end_headers()

    def _redirect_projector(self):
        self._redirect("/projector/index.html")

    def _redirect_stage(self):
        self._redirect("/stage/index.html")

    def _redirect_overview(self):
        self._redirect("/overview/index.html")

    def _send_response(self, mimetype, encoding, content):
        """Actually send a HTTP response with correct headers and encoding.
        """
        self.send_response(200)
        self.send_header('Content-type', mimetype)
        if encoding is not None:
            self.send_header('Content-Encoding', encoding)
        self.end_headers()
        self.wfile.write(content)

    def _index_html(self):
        self._send_file(os.path.join(build_config.SYSTEM_WEBSERVER_DIR, "index.html"), 'text/html')

    def _page_update_js(self):
        self._send_file(os.path.join(build_config.SYSTEM_WEBSERVER_DIR, "page_update.js"), 'application/javascript')

    def _favicon_png(self):
        self._send_file(os.path.join(build_config.REV_ICON_DIR, "favicon.png"), 'image/png')

    def _projector_index_html(self):
        self._send_file(os.path.join(build_config.SYSTEM_WEBSERVER_DIR, "projector.html"), 'text/html')

    def _projector_content_html(self):
        self._send_response('application/javascript', None, self.__projector_content_html)

    def _stage_index_html(self):
        self._send_file(os.path.join(build_config.SYSTEM_WEBSERVER_DIR, "stage.html"), 'text/html')

    def _stage_content_html(self):
        self._send_response('application/javascript', None, self.__stage_content_html)

    def _overview_index_html(self):
        self._send_file(os.path.join(build_config.SYSTEM_WEBSERVER_DIR, "overview.html"), 'text/html')

    def _overview_content_html(self):
        self._send_response('text/html', None, self.__overview_content_html)

    def _presentation_file(self, request_path):
        # Discard the leading /presentations/
        file_path = os.path.join(config.presentation_dir(), request_path[15:])
        if os.path.isfile(file_path):
            (mimetype, encoding) = mimetypes.guess_type(file_path)
            fd = open(file_path, 'rb')
            content = fd.read()
            fd.close()
            self._send_response(mimetype, encoding, content)
        else:
            self._notfound()
            return

    def _slide_def_json(self):
        # Supply some basic variables about the slide - this includes everything
        # outside the slide content, so that we don't hit up the server extra
        # times for tiny amounts of info.
        # Initialise vars to something valid
        timer_string = ""
        timer_class = "none"
        if (self.__timer_mode == "timer" or self.__timer_mode == "alarm"):
            time_remaining = self.__timer_end_time - datetime.datetime.today()
            if time_remaining > datetime.timedelta(minutes=5):
                timer_class = "plenty_of_time"
            elif time_remaining < datetime.timedelta(minutes=5):
                if time_remaining > datetime.timedelta(0):
                    timer_class = "nearly_time"
                else:
                    timer_class = "over_time"
            # Don't show decimal part of seconds
            if time_remaining.total_seconds() > 0:
                timer_string = str(time_remaining).split(".")[0]
            else:
                # Normalised timedeltas produce stuff like -1 day, 23 hours
                # which is just unintuitive and fairly ugly
                time_remaining = datetime.timedelta(seconds=(- time_remaining.total_seconds()))
                timer_string = "-" + str(time_remaining).split(".")[0]

        if self.__show_clock:
            clock_string = datetime.datetime.today().strftime("%H:%M:%S")
        else:
            clock_string = ""
        if self.__show_elapsed:
            # Don't show decimal part of seconds
            elapsed_string = str(datetime.datetime.today() - self.__elapsed_start_time).split(".")[0]
        else:
            elapsed_string = ""
        vars = {"slide_id": self.__slide_id,
            "overview_id": self.__overview_id,
            "projector_class": self.__projector_slide_class,
            "stage_class": self.__stage_slide_class,
            "theme": self.__theme_url_path,
            "timer_string": timer_string,
            "timer_class": timer_class,
            "clock_string": clock_string,
            "elapsed_string": elapsed_string,
            }
        json_str = json.dumps(vars)
        self._send_response('application/json', None, json_str.encode())

    def _send_file(self, path, mimetype):
        fd = open(path, 'rb')
        content = fd.read()
        fd.close()
        self._send_response(mimetype, None, content)

    @classmethod
    def set_slide_content(self, theme_url_path, projector_html, stage_html, projector_slide_class, stage_slide_class, timer_mode, timer_end_time, show_clock, show_elapsed, elapsed_start_time):
        """Update the current slide being served.

        theme_url_path: The URL path to the current theme.
        projector_html: The html to serve for the projector content.
        stage_html: The html to serve for the stage content.
        projector_slide_class: The html class of the projector slide.
        stage_slide_class: The html class of the stage slide.
        timer_mode: The mode of the timer (timer, alarm).
        timer_end_time: The end time for the timer and alarm modes.
        show_clock: Whether or not to show the clock.
        show_elapsed: Whether or not to show the elapsed time.
        elapsed_start_time: The starting time for the elapsed timer.
        """
        self.__theme_url_path = theme_url_path
        self.__projector_content_html = projector_html.encode()
        self.__stage_content_html = stage_html.encode()
        self.__projector_slide_class = projector_slide_class
        self.__stage_slide_class = stage_slide_class
        self.__timer_mode = timer_mode
        self.__timer_end_time = timer_end_time
        self.__show_clock = show_clock
        self.__show_elapsed = show_elapsed
        self.__elapsed_start_time = elapsed_start_time
        # self.__slide_id enables the browser to determine if the html has
        # changed.  It only has to check that the id is different - the
        # actual value doesn't really matter.
        self.__slide_id += 1
        if self.__slide_id >= 256:
            self.__slide_id = 0

    @classmethod
    def set_overview(self, overview_html):
        """Update the overview slide being served.

        overview_html: The html to serve.
        """
        self.__overview_content_html = overview_html.encode()
        # self.__overview_id enables the browser to determine if the html has
        # changed.  It only has to check that the id is different - the
        # actual value doesn't really matter.
        self.__overview_id += 1
        if self.__overview_id >= 256:
            self.__overview_id = 0

    @classmethod
    def init(self):
        """This classmethod exists so that some variables can be initialised
        before the server gets going, as it will start and begin processing
        HTTP requests before we can set them if we try to do it otherwise.
        """
        self.__theme_url_path = ""
        self.__projector_page_html = "<html><body><p>Webserver init page, you shouldn't be seeing this.</p></body></html>".encode()
        self.__projector_content_html = "".encode()
        self.__projector_slide_class = "default"
        self.__stage_page_html = "<html><body><p>Webserver init page, you shouldn't be seeing this.</p></body></html>".encode()
        self.__slide_id = 0
        self.__overview_id = 0
        self.__stage_content_html = "".encode()
        self.__stage_slide_class = "default"
        self.__overview_index_html = "<html><body><p>Webserver init page, you shouldn't be seeing this.</p></body></html>".encode()
        self.__overview_content_html = "".encode()
