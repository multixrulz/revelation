#!/usr/bin/python

"""the Revelation GUI, made with Qt."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt6 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets
from PyQt6 import uic
import sys, os, traceback
import subprocess
# Local / relative imports
from .qt_extensions import *
from . import app_lib
from . import config_qt
from . import editor_qt
from . import version
from .externalplayer import *


class RevMainWindow(QtWidgets.QMainWindow):
    """The Revelation main window.
    """

    def __init__(self, app, app_lib, *args, **kwargs):
        """Initialise the window.

        app: the Qt application object.
        app_lib: an instance of RevelationApp found in app_lib.
        """
        super().__init__(*args, **kwargs)
        self._app = app
        # Deal with application library
        self._app_lib = app_lib
        # Other initialisation
        self._current_slide = None # Must come before self._update_live() where it's used
        screens = self._app.screens()
        s0 = screens[0].geometry()
        self.move(s0.x(), s0.y())
        self.displayer = RevDisplayer(screens)
        self.displayer.mediaPlayer.media_started.connect(self._media_started)
        self.displayer.mediaPlayer.media_ended.connect(self._media_ended)
        self.displayer.external_started.connect(self._external_started)
        self.displayer.external_ended.connect(self._external_ended)
        self._init_gui()
        self._init_presentation_browser()
        self._init_event_browser()
        self._init_themes()
        self._load_previews(live=True)
        self.show()
        self._update_live()

    def _init_gui(self):
        """Load the QtCreator file and do GUI customisations."""
        uic.loadUi(os.path.join(build_config.UI_DIR, "RevelationMainWindow.ui"), self)
        # Create a delegate to render HTML content in the listview
        delegate = RevHtmlDelegate()
        self.listView_live.setItemDelegate(delegate)
        self.listView_preview.setItemDelegate(delegate)
        # Add actions to buttons - this allows them to work
        self.pb_first.addAction(self.actionFirst)
        self.pb_next.addAction(self.actionNext)
        self.pb_previous.addAction(self.actionPrevious)
        # Add icons to two buttons
        self.pb_freeze.setIcon(QtGui.QIcon(
            os.path.join(build_config.REV_ICON_DIR, "revelation-screen-freeze.svg")))
        self.pb_hide.setIcon(QtGui.QIcon(
            os.path.join(build_config.REV_ICON_DIR, "revelation-screen-hide.svg")))
        # Make listviews less jumpy
        self.listView_live.verticalScrollBar().setPageStep(4)
        self.listView_live.verticalScrollBar().setSingleStep(50)
        self.listView_preview.verticalScrollBar().setPageStep(4)
        self.listView_preview.verticalScrollBar().setSingleStep(50)

    def _init_themes(self):
        """Populate the theme combo box."""
        # Disconnect the combo box's signal so that we can change its contents
        # here without setting the theme.
        try:
            self.comboBox_theme.currentIndexChanged.disconnect()
        except TypeError:
            # This exception is thrown when there are no connections to disconnect
            # As far as I know, other solutions to this problem would be equally clunky.
            pass
        # Clear the box and start again.
        self.comboBox_theme.clear()
        # Get theme list
        themes = self._app_lib.themes()
        for (theme_path, url_path, theme_name) in themes:
            self.comboBox_theme.addItem(theme_name, theme_path)
        # Select the configured theme
        current_theme_path = config.current_theme_path()
        if not os.path.exists(current_theme_path):
            config.reset_theme_to_default()
            current_theme_path = config.current_theme_path()
        for i in range(self.comboBox_theme.model().rowCount()):
            if self.comboBox_theme.itemData(i) == current_theme_path:
                self.comboBox_theme.setCurrentIndex(i)
                break
        # Connect the combo box's signal now
        self.comboBox_theme.currentIndexChanged.connect(self._theme_changed)

    def _configure(self):
        """Open the configuration dialog."""
        config_qt.open_config_dialog()
        self._init_presentation_browser()
        self._init_event_browser()
        self._init_themes()

    def _theme_changed(self):
        """Update the presentation when the theme is changed.
            Save the theme selection."""
        if self.comboBox_theme.count() > 0:
            index = self.comboBox_theme.currentIndex()
            config.set_current_theme_path(self.comboBox_theme.itemData(index))
            self._app_lib.theme_changed()
            config.save()
            try:
                self._update_live()
            except AttributeError:
                # When loading the GUI, the theme changes and there is no presentation
                # yet.  If the presentation is loaded first, it fails because the GUI
                # isn't yet loaded.
                pass

    def _ev_pres_tab_changed(self):
        """Check that the buttons are set correctly when changing tabs."""
        self._set_buttons()

    def _init_event_browser(self):
        """Initialise the event treeview."""
        # Set a model and populate it
        fsmodel = RevFileSystemModel()
        fsmodel.setReadOnly(True)
        fsmodel.setResolveSymlinks(True)
        fsmodel.setNameFilters(["*.reve"])
        fsmodel.setNameFilterDisables(False)
        rootIndex = fsmodel.setRootPath(config.event_dir())
        self.treeView_event.setModel(fsmodel)
        self.treeView_event.setRootIndex(rootIndex)
        headerview = self.treeView_event.header()
        headerview.hideSection(1)
        headerview.hideSection(2)
        headerview.hideSection(3)
        self._event_fsmodel = fsmodel
        # Connect to the event selection to enable/disable event buttons
        selection_model = self.treeView_event.selectionModel()
        selection_model.selectionChanged.connect(self._set_buttons)
        selection_model = self.treeView_presentation.selectionModel()
        selection_model.selectionChanged.connect(self._set_buttons)
        self._set_buttons()

    def _event_filter_changed(self, new_string):
        """Filter the event treeview.

        new_string: the string of words to filter on.
        """
        filter = "*{}*".format(new_string)
        self._event_fsmodel.setNameFilters([filter,])

    def _event_activated(self, index):
        """Open the activated event.

        index: an index into the event treeview for the activated event
        """
        if self.tabWidget_views.currentWidget() == self.tab_live:
            file_info = self.treeView_event.model().fileInfo(index)
            self._app_lib.open(file_info.filePath())
            self._load_previews()
            self._listview_live_inactive_selection()
            self._update_listView_live()

    def _event_clicked(self, index):
        """Load the clicked event into the preview listview.

        index: an index into the event treeview for the clicked event
        """
        if self.tabWidget_views.currentWidget() == self.tab_preview:
            file_info = self.treeView_event.model().fileInfo(index)
            self._app_lib.open_preview(file_info.filePath())
            self._load_previews(live=False)

    def _pb_add_clicked(self):
        """Callback for the "add" pushbutton.  Create a new file.
        """
        create_event = self.tabWidget_files.currentWidget() == self.tab_event
        if create_event:
            # Create in the selected directory, if there is one
            rows = self.treeView_event.selectionModel().selectedRows()
        else:
            rows = self.treeView_presentation.selectionModel().selectedRows()
        if len(rows) == 1:
            if create_event:
                selected = self.treeView_event.model().fileInfo(rows[0])
            else:
                selected = self.treeView_presentation.model().fileInfo(rows[0])
            if selected.isFile():
                create_dir = selected.absolutePath()
            else:
                create_dir = selected.absoluteFilePath()
        else: # default to top-level dir otherwise
            if create_event:
                create_dir = config.event_dir()
            else:
                create_dir = config.presentation_dir()

        (filename, filter) = QtWidgets.QFileDialog.getSaveFileName(
            caption = "Create presentation file",
            directory = create_dir)
        if len(filename) > 0:
            if create_event:
                if not filename.endswith(".reve"):
                    filename = "{}.reve".format(filename)
            else:
                if not filename.endswith(".revp"):
                    filename = "{}.revp".format(filename)
        else:
            return
        # Create the file
        with open(filename, 'w') as fp:
            fp.write("% revelation")
        # Open it in the Revelation file editor
        self._edit_revfile(filename)

    def _pb_edit_clicked(self):
        """Callback for the "edit" pushbutton.  Open the file editor
            and load the selected file.
        """
        if self.tabWidget_files.currentWidget() == self.tab_event:
            # Edit an event
            file_info = self._get_selected_event_file()
            if file_info.isFile() and file_info.isReadable():
                # Only open .reve files
                path = file_info.absoluteFilePath()
                if not path.endswith(".reve"):
                    return
        elif self.tabWidget_files.currentWidget() == self.tab_presentation:
            # Edit a presentation
            file_info = self._get_selected_presentation_file()
            if file_info.isFile() and file_info.isReadable():
                # Only open .revp files
                path = file_info.absoluteFilePath()
                if not path.endswith(".revp"):
                    return
        self._edit_revfile(path)

    def _pb_editraw_clicked(self):
        """Callback for the "editraw" pushbutton.  Open the text file editor
            and load the selected file.
        """
        if self.tabWidget_files.currentWidget() == self.tab_event:
            # Edit an event
            file_info = self._get_selected_event_file()
            if file_info.isFile() and file_info.isReadable():
                # Only open .reve files
                path = file_info.absoluteFilePath()
                if not path.endswith(".reve"):
                    return
        elif self.tabWidget_files.currentWidget() == self.tab_presentation:
            # Edit a presentation
            file_info = self._get_selected_presentation_file()
            if file_info.isFile() and file_info.isReadable():
                # Only open .revp files
                path = file_info.absoluteFilePath()
                if not path.endswith(".revp"):
                    return
        with open(path) as f:
            # Not so space efficient, but we shouldn't be reading really big files
            text = f.read()
        editor = editor_qt.RevRawFileEditor(text)
        editor.exec()
        if editor.result() == QtWidgets.QDialog.DialogCode.Accepted:
            with open(path, 'w') as f:
                f.write(editor.text)

    def _pb_opendir_clicked(self):
        """Callback for the "open directory" pushbutton.  Open the selected
            directory in the default file manager."""
        if self.tabWidget_files.currentWidget() == self.tab_event:
            # Find the selected event directory
            file_info = self._get_selected_event_file()
        elif self.tabWidget_files.currentWidget() == self.tab_presentation:
            # Find the selected presentation directory
            file_info = self._get_selected_presentation_file()
        if file_info is not None: #ie a file is actually selected
            path = file_info.absoluteFilePath()
            if file_info.isFile():
                self._app_lib.open_directory(os.path.dirname(path))
            else:
                self._app_lib.open_directory(path)

    def _get_selected_event_file(self):
        """Return the currently selected event file.

        Returns a Qt fileInfo object.
        """
        rows = self.treeView_event.selectionModel().selectedRows()
        if len(rows) == 1:
            return self.treeView_event.model().fileInfo(rows[0])
        else:
            # This should never occur, probably should raise an exception if it does
            pass

    def _get_selected_presentation_file(self):
        """Return the currently selected presentation file.

        Returns a Qt fileInfo object.
        """
        rows = self.treeView_presentation.selectionModel().selectedRows()
        if len(rows) == 1:
            return self.treeView_presentation.model().fileInfo(rows[0])
        else:
            # This should never occur, probably should raise an exception if it does
            pass

    def _edit_revfile(self, filename):
        """Open the revelation file (.reve and revp) editor.

        filename: the path of the file to be opened.
        """
        editor = editor_qt.RevFileEditor(filename)
        editor.exec()

    def _set_buttons(self):
        """Update the opendir/add/edit buttons based on the
            event or presentation treeview's selection."""
        file_selected = False
        has_selection = False
        if self.tabWidget_files.currentWidget() == self.tab_event:
            rows = self.treeView_event.selectionModel().selectedRows()
            if len(rows) == 1:
                has_selection = True
                file_info = self.treeView_event.model().fileInfo(rows[0])
                if file_info.isFile() and file_info.isReadable():
                    file_selected = True
        else:
            rows = self.treeView_presentation.selectionModel().selectedRows()
            if len(rows) == 1:
                has_selection = True
                file_info = self.treeView_event.model().fileInfo(rows[0])
                if file_info.isFile() and file_info.isReadable():
                    path = file_info.absoluteFilePath()
                    if path.endswith(".revp"):
                        file_selected = True
        self.pb_edit.setEnabled(file_selected)
        self.pb_editraw.setEnabled(file_selected)
        self.pb_opendir.setEnabled(has_selection)

    def _init_presentation_browser(self):
        """Initialise the presentation treeview.
        """
        # Set a model and populate it
        fsmodel = RevFileSystemModel()
        fsmodel.setReadOnly(True)
        fsmodel.setResolveSymlinks(True)
        fsmodel.setNameFilterDisables(False)
        rootIndex = fsmodel.setRootPath(config.presentation_dir())
        self.treeView_presentation.setModel(fsmodel)
        self.treeView_presentation.setRootIndex(rootIndex)
        headerview = self.treeView_presentation.header()
        headerview.hideSection(1)
        headerview.hideSection(2)
        headerview.hideSection(3)
        self._presentation_fsmodel = fsmodel

    def _presentation_filter_changed(self, new_string):
        """Filter the presentation treeview.

        new_string: the string of words to filter on.
        """
        filter = "*{}*".format(new_string)
        self._presentation_fsmodel.setNameFilters([filter,])

    def _presentation_activated(self, index):
        """Open the activated presentation.

        index: an index into the presentation treeview for the activated presentation
        """
        if self.tabWidget_views.currentWidget() == self.tab_live:
            file_info = self.treeView_presentation.model().fileInfo(index)
            self._app_lib.open(file_info.filePath())
            self._load_previews()
            self._listview_live_inactive_selection()
            self._update_listView_live()

    def _presentation_clicked(self, index):
        """Load the clicked presentation into the preview listview.

        index: an index into the presentation treeview for the clicked presentation
        """
        if self.tabWidget_views.currentWidget() == self.tab_preview:
            file_info = self.treeView_presentation.model().fileInfo(index)
            self._app_lib.open_preview(file_info.filePath())
            self._load_previews(live=False)

    def _load_previews(self, live=True):
        """Load preview slides into the preview/live listview.

        live: True if the preview should be added to the live listview.
        """
        # Get the right previews
        if live:
            slides = self._app_lib.live_previews()
        else:
            slides = self._app_lib.preview_previews()
        # Load the slides into a model
        model = QtGui.QStandardItemModel()
        for s in slides:
            item = QtGui.QStandardItem(s)
            model.appendRow(item)
        # Attach the model to the correct widget
        if live and self.tabWidget_views.currentWidget() == self.tab_live:
            self.listView_live.setModel(model)
            self._model_live = model
        elif not live and self.tabWidget_views.currentWidget() == self.tab_preview:
            self.listView_preview.setModel(model)

    def _freeze_clicked(self):
        """Freeze/unfreeze the event/presentation when the freeze button
            is clicked."""
        self._app_lib.set_freeze_state(self.pb_freeze.isChecked())
        if self.pb_freeze.isChecked():
            # Change the colour of the selection so it's obvious that something is different
            self._listview_live_inactive_selection()

    def _listview_live_inactive_selection(self):
        """Set a colour on the live listview selection to indicate that it is not actually displayed"""
        self.listView_live.setStyleSheet("QListView::item:selected { background: palette(Mid) }")
        self._selected_slide_active = False

    def _listview_live_active_selection(self):
        """Set a colour on the live listview selection to indicate that it is actually displayed"""
        self.listView_live.setStyleSheet("QListView::item:selected { background: palette(Highlight) }")
        self._selected_slide_active = True

    def _update_live(self, media_inhibit=False):
        """Update the projector output and live listview.

        media_inhibit: True if media on the new slide should not be played automatically
        """
        self._update_listView_live()
        if not self.pb_freeze.isChecked():
            new_slide = self._app_lib.current_slide()
            self._current_slide = new_slide
            self.displayer.update(new_slide, media_inhibit)
            self._listview_live_active_selection()

    def _hide_clicked(self):
        """Hide/unhide the projector output window.
        """
        self.displayer.setVisible(not self.pb_hide.isChecked())

    def _live_first(self):
        """Go to the first slide of the live event/presentation.
        """
        self._app_lib.first_slide()
        self._update_live()

    def _live_next(self):
        """Go to the next slide of the live event/presentation.
        """
        if self._app_lib.next_slide(): # If the slide actually changed
            self._update_live()

    def _live_previous(self):
        """Go to the previous slide of the live event/presentation.
        """
        self._app_lib.previous_slide()
        self._update_live(media_inhibit=True)

    def _listView_live_clicked(self):
        """Show the clicked slide.
        """
        sm = self.listView_live.selectionModel()
        indexes = sm.selectedRows()
        if len(indexes) > 0:
            rownum = indexes[0].row()
            self._app_lib.go_slide(rownum)
            self._update_live()

    def _media_beginning(self):
        """Go to the beginning of the media file - the beginning button
            was clicked.
        """
        self.displayer.media_beginning()
        self._set_media_widget_states()

    def _media_pause(self):
        """Pause the media file - the pause button was clicked.
        """
        self.displayer.media_pause_play()
        self._set_media_widget_states()

    def _media_stop(self):
        """Stop the media file - the stop button was clicked.
        """
        self.displayer.media_stop()
        self._set_media_widget_states()

    def _media_started(self, media_name, show_video, advance):
        """Update widgets when media starts playing.

        media_name: the name of the media file that has started playing.
        is_video: True if the media is video.
        advance: True if the next slide should be shown immediately.
        """
        self.label_mediafile.setText(media_name)
        self._set_media_widget_states()
        if advance:
            self._live_next()

    def _media_ended(self, advance):
        """Update widgets when media ends playing.  Advance a slide if
        the media ended_naturally.

        advance: True if the next slide should be shown immediately.
        """
        self._set_media_widget_states()
        if advance and self._selected_slide_active:
            self._live_next()

    def _set_media_widget_states(self):
        """Check and update the mediaplayer and update widget states.
        """
        self.pb_beginning.setEnabled(self.displayer.mediaPlayer.playing())
        self.pb_stop.setEnabled(self.displayer.mediaPlayer.playing())
        self.pb_pause.setEnabled(self.displayer.mediaPlayer.playing())
        if self.displayer.mediaPlayer.paused():
            self.pb_pause.setIcon(QtGui.QIcon.fromTheme('media-playback-start'))
        else:
            self.pb_pause.setIcon(QtGui.QIcon.fromTheme('media-playback-pause'))
        if not self.displayer.mediaPlayer.playing():
            self.label_mediafile.setText("No media")

    def _external_started(self):
        """Update widgets when the external content starts playing.

        advance: True if the next slide should be shown immediately.
        """
        self.displayer.hide()

    def _external_ended(self):
        """Update widgets when the external content ends.

        advance: True if the next slide should be shown immediately.
        """
        self.displayer.show()
        self._live_next()

    def _update_listView_live(self):
        """Update the live listview so that the current slide is selected.
        """
        current_slide = self._app_lib.current_slide_number()
        sm = self.listView_live.selectionModel()
        sm.clear()
        index = self._model_live.index(current_slide, 0)
        sm.select(index,
            QtCore.QItemSelectionModel.SelectionFlag.Select |
            QtCore.QItemSelectionModel.SelectionFlag.Rows)
        # Scroll to the row
        self.listView_live.scrollTo(index)

    def _show_about(self, event):
        """Show and about dialog.

        event: an event object from the widget that triggered this method.
        """
        about_dialog = RevAboutDialog()
        about_dialog.exec()

    def closeEvent(self, event):
        """Clean up when the window is closed.

        event: an event object from the widget that triggered this method.
        """
        self.cleanup()

    def cleanup(self):
        """Clean up when quitting.
        """
        self._app_lib.quit()
        self.displayer.close_window()

    def quit(self):
        """Close the window."""
        self.close()

class RevDisplayer(QtWidgets.QDialog):
    """The Revelation display window, that is sent to the projector.
    """

    external_started = QtCore.pyqtSignal()
    external_ended = QtCore.pyqtSignal()

    def __init__(self, screens, *args, **kwargs):
        """Initialise the window.

        screens: a list of Qt screen objects obtained from the application.
        """
        super().__init__(*args, **kwargs)

        # Load the Ui
        uic.loadUi(os.path.join(build_config.UI_DIR, "RevelationDisplayer.ui"), self)
        self._convert_widgets()
        self.externalPlayer = RevExternalPlayer(self._external_started_callback, self._external_ended_callback)
        # ### Dual screen and window flags etc
        # Make the window behave as we need: fullscreen on the second-monitor
        # if there is one, no closing, and no taskbar icon/button
        self.allow_close = False
        self.setStyleSheet("background: black;")
        window_flags = QtCore.Qt.WindowType.WindowDoesNotAcceptFocus
        if len(screens) > 1:
            # Displayer goes on screen 1 - this is a bit hacky, but less so than the qt5 version
            self.setGeometry(screens[1].geometry())
            self.setWindowState(QtCore.Qt.WindowState.WindowFullScreen)
            window_flags |= QtCore.Qt.WindowType.FramelessWindowHint
            # This doesn't work:
            # window_flags |= QtCore.Qt.WindowType.WindowStaysOnTopHint
            # This seems to make it stay on top
            window_flags |= QtCore.Qt.WindowType.X11BypassWindowManagerHint
        self.setWindowFlags(window_flags)
        self.webEngineView.setUrl(QtCore.QUrl("http://localhost:8080/projector/index.html"))
        self.show()

    def resizeEvent(self, event):
        """Resize the contained widgets to the window size when the window is resized.
        """
        size = event.size()
        rect = QtCore.QRect(0, 0, size.width(), size.height())
        self.webEngineView.setGeometry(rect)
        self.mediaPlayer.setGeometry(rect)

    def _convert_widgets(self):
        """Convert from standard Qt widgets to Revelation-specific ones.
        """
        RevWebView.create_from(self.webEngineView)
        RevMediaWidget.create_from(self.mediaPlayer)

    def update(self, slide, media_inhibit):
        """Display a new slide. HTML slides are served via the webserver.
        This function just has to display or hide the HTML-displaying widget.

        slide: the slide to display.
        media_inhibit: do not play media on this slide if true.
        """

        # Stop currently-playing synchronous media
        if self.mediaPlayer.playing():
            if self.mediaPlayer.synchronous():
                self.media_stop()

        # Play media, and show it if there's video
        if slide.is_media():
            if not media_inhibit or slide.always_play():
                self.mediaPlayer.play(slide)
                if slide.media_show_video():
                    self.mediaPlayer.show()
        else:
            self.mediaPlayer.hide()
        if slide.is_external():
            self.externalPlayer.play(slide)

    def media_pause_play(self):
        """Pause/play the slide's media.
        """
        self.mediaPlayer.pause_play()

    def media_beginning(self):
        """Play the slide's media from the beginning.
        """
        self.mediaPlayer.beginning()

    def media_stop(self):
        """Stop the slide's media.
        """
        self.webEngineView.show()
        self.mediaPlayer.hide()
        self.mediaPlayer.stop()

    def close_window(self):
        """Closes the window.
        """
        self.mediaPlayer.stop()
        self.allow_close = True
        self.close()

    def closeEvent(self, event):
        """Re-implement the closeEvent method to prevent this window
        from being closed at normal times.
        """
        # Don't allow the window to be closed unless we actually want it closed
        if self.allow_close != True:
            event.ignore()

    def keyPressEvent(self, event):
        """Re-implement keyPressEvent to ignore all keypresses, as this
        window is for display only.
        """
        # Ignore all keypresses, ESPECIALLY escape (which closes dialogs)
        event.ignore()

    # This method exists to work around Qt.  A callback from a different thread
    # (as this one is) cannot modify widgets
    def _external_ended_callback(self):
        """Convert a callback method into a Qt signal.

        reached_end: True if media reached the end.
        """
        self.external_ended.emit()

    # This method exists to work around Qt.  A callback from a different thread
    # (as this one is) cannot modify widgets
    def _external_started_callback(self):
        """Convert a callback method into a Qt signal.
        """
        self.external_started.emit()


class RevAboutDialog(QtWidgets.QDialog):
    """The Revelation about dialog window."""
    def __init__(self, *args, **kwargs):
        """Initialise the window.
        """
        super().__init__(*args, **kwargs)
        # Load the QtCreator file
        uic.loadUi(os.path.join(build_config.UI_DIR, "about.ui"), self)
        self.label_version.setText("Revelation {}".format(version.version))
        app_icon = QtGui.QPixmap(os.path.join(build_config.SYSTEM_ICON_DIR, 'revelation-icon.svg'))
        self.label_icon.setPixmap(app_icon)
        self.adjustSize() # Resize to fit contents
        self.show()


class RevelationAppQt():
    def run(self):
        """Execute the Revelation program.
        """
        # Create an application object
        self.app = QtWidgets.QApplication(sys.argv)

        # Set a custom exception hook in order to display a nice message
        # dialog when an exception occurs.
        sys.excepthook = self._custom_exception_hook

        # Check the configuration is OK
        config_qt.check_config()

        # Create the application library.  It has to be here in case an
        # exception occurs while creating the Qt app but after the http
        # server (within the app_lib) has been started, so that the http
        # server can still be stopped.
        self._app_lib = None # Recover easily if an exception occurs in constructor
        self._app_lib = app_lib.RevelationApp()

        # Set an icon for the application
        app_icon = QtGui.QIcon(os.path.join(build_config.SYSTEM_ICON_DIR, 'revelation-icon.svg'))
        self.app.setWindowIcon(app_icon)

        # Style the app
        self.app.setStyleSheet(
        """
        * {selection-background-color: palette(Highlight)}
        """)

        # Create and show the main window -- the window shows itself
        self.main_window = RevMainWindow(self.app, self._app_lib)

        # Execute the application -- get the event loop running
        return self.app.exec()

    def _custom_exception_hook(self, type, value, actual_traceback):
        """Define a customised sys.excepthook to display exceptions
        nicely to the user.

        See python documentation for what the arguments are.
        """
        traceback_string = "".join(traceback.format_exception(type, value, actual_traceback))
        sys.stderr.write(traceback_string)
        sys.stderr.flush()
        primary_text = "That's a bug."
        secondary_text = "We're very sorry, but Revelation will now close.  The details below will be helpful to the developers.<br><br>{}".format(value)

        # Display the error in a message box
        msgbox = QtWidgets.QMessageBox()
        msgbox.setWindowTitle("Revelation Error")
        msgbox.setIcon(QtWidgets.QMessageBox.Icon.Critical)
        msgbox.setText("<h1>{}</h1>{}".format(primary_text, secondary_text))
        msgbox.setDetailedText(traceback_string)
        msgbox.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Close)
        msgbox.setDefaultButton(QtWidgets.QMessageBox.StandardButton.Close)
        result = msgbox.exec()
        if hasattr(self, 'main_window'):
            self.main_window.cleanup() # Do any housekeeping required
        else:
            if self._app_lib is not None:
                self._app_lib.quit()
        self.app.exit()


def exec():
        """
        Execute the Revelation program.

        Simply import this module and execute this exec() function.
        """
        rev_app = RevelationAppQt()
        rev_app.run()
