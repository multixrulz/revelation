#!/usr/bin/python

"""This module sets some variables appropriately for the development environment.
They are then modified by setup.py to reflect the install environment.
"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

# The following definitions will be overwritten by distutils at install time

UI_DIR = os.path.join(os.path.dirname(__file__), "..", "ui")
SYSTEM_ICON_DIR = os.path.join(os.path.dirname(__file__), "..", "other", "icons")
REV_ICON_DIR = os.path.join(os.path.dirname(__file__), "..", "other", "icons")
SYSTEM_THEME_DIR = os.path.join(os.path.dirname(__file__), "..", "other", "themes")
SYSTEM_WEBSERVER_DIR = os.path.join(os.path.dirname(__file__), "..", "other", "webserver")
