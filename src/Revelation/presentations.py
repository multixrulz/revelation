#!/usr/bin/python
"""Classes for Revelation presentation types.
"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import subprocess
import markdown
import urllib.parse
import re
from string import Template
import datetime
# Local / relative imports
from . import config
from . import mime
from .exceptions import *


def open_slideshow(filepath):
    """Opens a slideshow.

    filepath: the path to the file to open.

    Returns a RevSlideshow object.
    """
    if not os.path.isfile(filepath):
        error_message = "The specified file does not exist"
        return RevSlideshow([RevSlideError(filepath, error_message),])

    mimetype = mime.mimetype(filepath)
    if mimetype.media == 'text':
        extension = os.path.splitext(filepath)[1]
        if extension == '.revp':
            rev_file = RevPres(filepath)
        elif extension == '.reve':
            rev_file = RevEvent(filepath)
        else:
            error_message = "This is not a supported file type."
            return RevSlideshow([RevSlideError(filepath, error_message),])
    else:
        # Whip up a revelation file and let RevFile handle it
        # The file needs to be / must be in the presentation directory,
        # so remove that prefix
        relpath = os.path.relpath(filepath, config.presentation_dir())
        rev_content = ["% revelation", "% file {}".format(relpath)]
        rev_file = RevPresTemp(content=rev_content)
    return rev_file.slideshow()

def is_new_slide(line):
    """Returns True if line is a slide definition.

    line: a line from a Revelation file.
    """
    return line.startswith('%') and not line.startswith('%%')

def is_new_target(line):
    """Returns True if line is a target definition.

    line: a line from a Revelation file.
    """
    return line.startswith('@') and not line.startswith('@@')

def parse_file(filepath):
    """Parses a Revelation file and does basic sanity checking
    and unescaping on it.

    filepath: The path of the file to read.

    Returns a tuple of raw slides where each element is a tuple:
        (slide_type, slide_argument, slide_content).
    """

    with open(filepath, 'r') as f:
        lines = f.read().splitlines()
    return parse_file_content(lines)

def parse_file_content(lines):
    """Parses the content of a Revelation file and does basic sanity checking
    and unescaping on it. Call this function if you have file content in memory.

    lines: A list containing all the lines in the file.

    Returns a tuple of raw slides where each element is a tuple:
        (slide_type, slide_argument, slide_content).
    """
    ### Basic checks on the file content
    # Could be an empty presentation without the magic first line
    if len(lines) == 0:
        return ()
    # Check that the first line contains a revelation signature
    if re.match(r'%\s*revelation$', lines[0]) is None:
        raise RevError("Line 1 does not start with the Revelation file signature '% revelation'.")
    # Could be an empty presentation
    if len(lines) == 1:
        return ()
    # Process the rest of the file
    if not is_new_slide(lines[1]):
        raise RevError("Line 2 is not a new slide definition (ie it doesn’t start with '% slidetype')")

    ### The file starts right, let's process it
    content = lines[1:]
    # Contains tuples of (str, list) where str is the slide definition and
    # list is all the lines it contains
    slidetext = []
    for line in content:
        if is_new_slide(line):
            slidetext.append((line, []))
        else:
            # Apply %% unescaping
            if line.startswith('%%'):
                line = line[1:]
            slidetext[-1][1].append(line)

    raw_slides = []
    for st in slidetext:
        raw_slides.append(parse_slide(st))
    return (raw_slides)

slide_def_re = re.compile(r'%\s*([a-zA-Z#]+)\s*(.*)\s*')
new_target_re = re.compile(r'@\s*([a-zA-Z]+)\s*(.*)\s*')
def parse_slide(slide_data):
    """Parses a Revelation slide from a file.

    slide_data: the entire slide from the Revelation file.

    Returns a raw slide which is a tuple: (slide_type, slide_argument, slide_content).
    """
    (slide_def, content) = slide_data
    m = slide_def_re.match(slide_def)
    slide_type, slide_argument = m.groups() if m is not None else ('error', slide_def)
    # Make the implicit target explicit, but not if there's no content at all
    if len(content) > 0:
        if not is_new_target(content[0]):
            content.insert(0, "@projector")
    # Keys are the label after the @, values are lists of all the lines
    # it contains
    targets = dict()
    for line in content:
        if is_new_target(line):
            m = new_target_re.match(line)
            target, style = m.groups() if m is not None else ('', '')
            targets[target] = {'style': style, 'content': []}
        else:
            # Apply @@ unescaping
            if line.startswith('@@'):
                line = line[1:]
            targets[target]['content'].append(line)
    if 'projector' not in targets.keys():
        # Ensure there is always projector content
        targets['projector'] = {'style': '', 'content': []}
    for k in targets.keys():
        # Join the lines in the list together
        targets[k]['content'] = "\n".join(targets[k]['content'])
    return (slide_type, slide_argument, targets)

def escape_content(content):
    """Escapes % and @ within a Revelation slide's content.

    content: the text of the slide.

    Returns the escaped content.
    """
    lines = content.splitlines()
    for i, line in enumerate(lines):
        if is_new_slide(line):
            lines[i] = '%{}'.format(line)
        if is_new_target(line):
            lines[i] = '@{}'.format(line)
    return "\n".join(lines)

def slide_text(raw_slide):
    """Converts a raw slide into the text that is saved in a Revelation file.

    raw_slide: a raw slide (ie the tuple (slide_type, slide_argument, slide_content))

    Returns the text to save into a Revelation file.
    """
    (slide_type, slide_argument, targets) = raw_slide
    texts = ["% {} {}".format(slide_type, slide_argument).strip()]
    # Projector first.
    if 'projector' in targets.keys():
        # Explicit projector target - make implicit if there's no style.
        if len(targets['projector']['style']) > 0:
            texts.append("@projector {}\n{}".format(targets['projector']['style']))
        texts.append(escape_content(targets['projector']['content']))
    # Stage and mediaplayer targets are supported - write out in this order.
    for t in ['stage', 'mediaplayer']:
        if t in targets.keys():
            texts.append("@{} {}".format(t, targets[t]['style']).strip())
            texts.append(escape_content(targets[t]['content']))
    # Write out unsupported targets
    for t in targets.keys():
        if t not in ['projector', 'stage', 'mediaplayer']:
            texts.append("@{} {}".format(t, targets[t]['style']).strip())
            texts.append(escape_content(targets[t]['content']))
    return "\n".join(texts)

def parse_time(value):
    """
    convert input string to a time
    """

    parts = value.split(":")
    parts.reverse() # List parts in reverse order
    part_order = ("second", "minute", "hour")
    try:
        part_dict = {k: int(v) for v, k in zip(parts, part_order)}
    except ValueError:
        return None
    else:
        return datetime.time(**part_dict)

def parse_timedelta(value):
    """
    convert input string to a time
    """

    parts = value.split(":")
    parts.reverse() # List parts in reverse order
    part_order = ("seconds", "minutes", "hours")
    try:
        part_dict = {k: int(v) for v, k in zip(parts, part_order)}
    except ValueError:
        return None
    else:
        return datetime.timedelta(**part_dict)

class RevFile():
    """Handles a revelation-formatted file"""
    def __init__(self, filepath=None, content=None):
        """Read in a revelation-formatted file.

        filepath: The path of the file to read.
        content: Supply some file content if there's no actual file to read.
        """
        self._filepath = filepath
        self._slides = []

        slides_created = False
        if filepath is None:
            if content is None: # Create a presentation with one empty slide
                self._slides = [RevSlideNull(),]
                slides_created = True
            else: # Parse the supplied content
                raw_slides = parse_file_content(content)
        else: # Parse the file - first check the extension
            if not self._own_path_ok():
                slide_callable = RevSlideError
                slide_content = "The filepath given is not permitted—for .reve it must be within the events directory; for .revp it must be in the presentations directory.".format(filepath)
                self._slides = [RevSlideError(filepath, slide_content),]
                slides_created = True
            # We're good to read in the file
            try:
                raw_slides = parse_file(filepath)
            except RevError as e:
                slide_content = str(e)
                self._slides.append(RevSlideError(filepath, slide_content))
                slides_created = True
        if not slides_created:
            # We have some raw slides, now handle them
            slidemap = {
                'markdown': RevSlideMarkdown,
                'md': RevSlideMarkdown,
                '#': RevSlideComment,
                'media': self.__mime_selector,
                'image': self.__mime_selector,
                'revp': self.__mime_selector,
                'file': self.__mime_selector,
                'error': RevSlideError,
                '': RevSlideError
                }
            for rs in raw_slides:
                (slide_type, slide_argument, slide_content) = rs
                try:
                    slide_callable = slidemap[slide_type]
                except KeyError:
                    slide_callable = RevSlideError
                    slide_content = "There's no slide type '{}' known to Revelation.".format(slide_type)
                self._slides.append(slide_callable(slide_argument, slide_content))

    def __mime_selector(self, filepath, slide_content):
        # filepath is supplied by the revelation file or the path of the
        # file that was directly opened
        slide_filepath = os.path.join(self._path_base(), filepath)
        try:
            # Do some basic checks
            if not self._path_ok(slide_filepath):
                error_message = "The filepath given is not permitted—for events it must be within the presentations directory; for presentations it must be in the same directory or a subdirectory."
                raise RevError
            elif not os.path.exists(slide_filepath):
                error_message = "The file does not exist."
                raise RevError
            else:
                ### Detect the mimetype and open appropriately
                error_message = "This is not a supported file type."
                mimetype = mime.mimetype(slide_filepath)
                # Text mimetypes
                if mimetype.media == 'text':
                    extension = os.path.splitext(slide_filepath)[1]
                    if extension == '.revp':
                        return RevPres(slide_filepath)
                    elif extension == '.reve':
                        return RevEvent(slide_filepath)
                    else:
                        raise RevError
                # Images - only accept HTML-supported image types
                elif mimetype.media == 'image':
                    if mimetype.subtype in ('jpeg', 'gif', 'png', 'svg+xml'):
                        return RevSlideImage(slide_filepath, slide_content)
                    else:
                        raise RevError
                # Video & Audio
                elif mimetype.media == 'video' or mimetype.media == 'audio':
                    return RevSlideMedia(slide_filepath, slide_content)
                # Things that external programs open
                elif mimetype.media == 'application':
                    if mimetype.subtype in ('pdf',
                        'vnd.oasis.opendocument.presentation',
                        'vnd.ms-powerpoint',
                        'vnd.openxmlformats-officedocument.presentationml.presentation'):
                        return RevSlideExternal(slide_filepath, mimetype.subtype, slide_content)
                    else:
                        raise RevError
                #Everything else - unknown presentation
                else:
                    raise RevError
        except RevError:
            return RevSlideError(filepath, error_message)

    def slideshow(self):
        """Returns a RevSlideshow object corresponding to the file.
        """
        return RevSlideshow(self.slides())

    def slides(self):
        """Returns a list of all the slides in all of the objects in the slides list.
        """
        slidelist = []
        for s in self._slides:
            slidelist.extend(s.slides())
        return slidelist

    def _own_path_ok(self):
        """Returns True if the filepath is in the configured event or presentation directory.
        """
        base_dir = self._self_path_base()
        normalised = os.path.normpath(self._filepath)
        common = os.path.commonprefix([base_dir, normalised])
        return common == base_dir

    def _path_ok(self, filepath):
        """Returns True if the filepath is valid for a presentation within this presentation.

        filepath: the filepath of a presentation within this presentation.
        """
        base_dir = self._path_base()
        normalised = os.path.normpath(filepath)
        common = os.path.commonprefix([base_dir, normalised])
        return common == base_dir


class RevPresTemp(RevFile):
    """A Revelation presentation"""
    def _self_path_base(self):
        """Returns the path to compare against for _own_path_ok() - the presentation directory.
        """
        # Actually this is a furphy because there is no physical file
        # involved.
        return config.presentation_dir()

    def _path_base(self):
        """Returns the base path to compare against for components of this presentation."""
        # For temporary (in-memory) presentations, the base path is the
        # presentations directory.
        return config.presentation_dir()


class RevPres(RevFile):
    """A Revelation presentation"""
    def _self_path_base(self):
        """Returns the path to compare against for _own_path_ok() - the presentation directory.
        """
        return config.presentation_dir()

    def _path_base(self):
        """Returns the base path to compare against for components of this presentation."""
        # For presentations, files are relative to the directory of the
        # containing presentation
        return os.path.dirname(self._filepath)


class RevEvent(RevFile):
    """An event."""
    def _self_path_base(self):
        """Returns the path to compare against for _own_path_ok() - the event directory.
        """
        return config.event_dir()

    def _path_base(self):
        """Returns the base path to compare against for components of this event."""
        return config.presentation_dir()


class RevSlideshow():
    def __init__(self, slidelist):
        self.slides = slidelist
        self._current_slide = 0
        self._last_slide = len(self.slides) - 1

    def go_next_slide(self):
        """Advance to the next slide.  If we're already on the last slide,
        do nothing.

        Returns true if the slide was advanced, false if already on the
        last slide.
        """
        if self._current_slide < self._last_slide:
            self._current_slide += 1
            return True
        else:
            return False

    def go_previous_slide(self):
        """Retreat to the previous slide.  If we're already on the first
        slide, do nothing."""
        if self._current_slide > 0:
            self._current_slide -= 1

    def go_first_slide(self):
        """Go to the first slide."""
        self._current_slide = 0

    def go_last_slide(self):
        """Go to the last slide."""
        self._current_slide = self._last_slide

    def go_slide(self, slide_number):
        """Set the current slide to the specified slide number, or do
        nothing if the slide number specified is out of range.

        slide_number: the slide number to go to.
        """
        if slide_number >= 0 and slide_number <= self._last_slide:
            self._current_slide = slide_number

    def current_slide_number(self):
        """Return the slide number for the current slide."""
        return self._current_slide

    def current_slide(self):
        """Return the current slide."""
        return self.slides[self._current_slide]

    def overview(self):
        """Return the HTML to send to the browser for the overview of this presentation.
        """
        h = "\n".join(['<div class="{}">{}</div>'.format(
            s.style('overview'), s.html('overview'))
            for s in self.slides])
        return h


class RevSlide():
    """Basic presentation class implemented by all presentation types."""
    def __init__(self, arg, targets=None, *args):
        """Initialise the class."""
        self._arg = arg

        # Fix up the content
        if targets is None:
            targets = {'projector': {'style': '', 'content': ''}}
        # Process the clock target
        self._timer_mode = None
        self._timer_duration = None
        self._timer_alarm = None
        self._show_clock = None
        self._show_elapsed = None
        midnight = datetime.time(hour=0, minute=0, second=0)
        if 'clock' in targets.keys():
            clockconfig = targets['clock']['content']
            matches = re.finditer(r'\s*([a-zA-Z]+)\s*:\s*(.+)\s*', clockconfig)
            for m in matches:
                key, val = m.groups()
                key = key.lower()
                if key == 'mode':
                    self._timer_mode = val
                elif key == 'duration':
                    duration = parse_timedelta(val)
                    self._timer_duration = duration;
                elif key == 'alarm':
                    alarm_time = parse_time(val)
                    alarm_datetime = datetime.datetime.combine(
                        datetime.datetime.today().date(),
                        alarm_time)
                    self._timer_alarm = alarm_datetime
                elif key == 'clock':
                    if val.lower() == 'on':
                        self._show_clock = True
                    if val.lower() == 'off':
                        self._show_clock = False
                elif key == 'elapsed':
                    if val.lower() == 'on':
                        self._show_elapsed = True
                    if val.lower() == 'off':
                        self._show_elapsed = False
        self._targets = targets

    def style(self, target):
        """Get the style of the specified target.

        target: the target to get info for.

        Returns the style of the target.
        """
        return self._target_info(target, True)

    def content(self, target):
        """Get the content of the specified target.

        target: the target to get info for.

        Returns the content of the target.
        """
        return self._target_info(target, False)

    def _gen_content(self):
        """Generate slide content to use when nothing is supplied.
        """
        return ''

    def _target_info(self, target, style):
        """Get information about the specified target.

        target: the target to get info for.
        style: True if the style of the target is desired, False if the content is.

        Returns the style or content of the target, as specified by style.
        """
        info = self._gen_content()
        try:
            info = self._targets[target]['style'] if style else self._targets[target]['content']
        except KeyError:
            fallbacks = {'stage': ['projector',],
                'preview': ['projector',],
                'overview': ['stage', 'projector']}
            try:
                for fb in fallbacks[target]:
                    try:
                        info = self._targets[fb]['style'] if style else self._targets[fb]['content']
                        break
                    except KeyError:
                        continue
            except KeyError:
                pass
        return info

    def slides(self):
        """Return a list of all the slides - for most things this'll be itself, but for
        comments it's an empty list and for presentations it's all the slides in the
        presentation.
        """
        return [self,]

    def html(self, target):
        """Convert the slide to HTML.

        target: the target to get the HTML for.

        Returns the HTML representation of the slide for the specified target, but not
            a complete HTML document.
        """
        return self._markdown(self.content(target))

    def _markdown(self, md):
        return markdown.markdown(md ,
            extensions=['fenced_code', 'smarty', 'extra', 'legacy_em', 'sane_lists'])

    def is_media(self):
        """Return True if the slide is a media slide.
        """
        return False

    def is_external(self):
        """Return True if the slide is opened by an external program.
        """
        return False

    def __repr__(self):
        lines = [
            "@{} {}".format(key, self._targets[key]['style']) + (
                "\n" + self._targets[key]['content']
                    if len(self._targets[key]) > 0
                    else "")
            for key in self._targets.keys()
            ]
        content = "\n".join(lines)
        return "{}('{}', '{}')".format(
            self.__class__.__name__,
            self._arg,
            content)


class RevSlideMarkdown(RevSlide):
    """Text presentation."""
    def __init__(self, style, content):
        """Initialise the presentation and read the file in.

        style: the style of the slide.
        content: the contents of the slide straight from the Revelation file.
        """
        super().__init__(style, content)
        # Apply default style when it's not defined for a target
        for target in self._targets.keys():
            if self._targets[target]['style'] == '':
                self._targets[target]['style'] = style


class RevSlideNull(RevSlide):
    """An empty presentation."""
    def __init__(self,):
        """Initialise the presentation."""
        super().__init__('', dict())


class RevSlideError(RevSlide):
    """A "file couldn't be opened" presentation."""
    def __init__(self, filepath, errortext):
        """Initialise the presentation."""
        # Change the error text to something slightly informative
        if errortext == '' or errortext is None:
            errortext = 'There is an error with the slide.  All I have for you is "{}"'.format(filepath)
        errortext = ['# Error', '', filepath, errortext]
        raw_slide = parse_slide(['% error {}'.format(filepath), errortext])
        slide_type, slide_argument, targets = raw_slide
        super().__init__(filepath, targets)
        # Change all styles to "error" so that it gets displayed properly
        for t in self._targets.keys():
            self._targets[t]['style'] = "error"


class RevSlideMedia(RevSlide):
    """A media presentation, with additional parameters defined."""
    def __init__(self, filepath, content=None):
        # Do generic initialisations
        super().__init__(filepath, content)

        # Generic defaults
        self._media_type = "media"
        self._media_audio_only = False
        self._media_path = filepath
        self._media_name = os.path.splitext(os.path.split(filepath)[1])[0]
        self._start = None
        self._end = None
        self._volume = None
        self._synchronous = True
        self._advance = False # If true: don't display this slide, just play the media and go ot the next one.
        self._loop = None
        self._always_play = False # If false: playing media when going backwards through slides is undesirable.

        # Are we audio or video, change the defaults
        mimetype = mime.mimetype(filepath)
        self._media_type = mimetype.media
        if mimetype.media == 'video':
            self._media_audio_only = False
            self._synchronous = True
            self._advance = True
        elif mimetype.media == 'audio':
            self._media_audio_only = True
            self._synchronous = False
            self._advance = True
        elif mimetype.media == 'image':
            self._media_audio_only = False
            self._loop = True
            self._always_play = True

        def str2bool(string):
            true = ('true', '1', 'yes')
            false = ('false', '0', 'no')
            if string.lower() in true:
                return True
            if string.lower() in false:
                return False
            raise ValueError(string)

        if 'mediaplayer' in self._targets.keys():
            mediaconfig = self._targets['mediaplayer']['content']
            matches = re.finditer(r'\s*([a-zA-Z]+)\s*:\s*(.+)\s*', mediaconfig)
            for m in matches:
                key, val = m.groups()
                key = key.lower()
                if key == 'name':
                    self._media_name = val
                elif key == 'start':
                        self._start = val
                elif key == 'end':
                        self._end = val
                elif key == 'volume':
                    try:
                        self._volume = float(val)
                    except ValueError:
                        pass # Ignore the failure
                elif key == 'synchronous':
                    try:
                        self._synchronous = str2bool(val)
                    except ValueError:
                        pass # Ignore the failure
                elif key == 'advance':
                    try:
                        self._advance = str2bool(val)
                    except ValueError:
                        pass # Ignore the failure
                elif key == 'loop':
                    try:
                        self._loop = str2bool(val)
                    except ValueError:
                        pass # Ignore the failure

    def _gen_content(self):
        """Generate slide content to use when nothing is supplied.
        """
        gen_content = "# {}\n\n({})".format(self._media_name, self._media_type)
        if not self._synchronous:
            gen_content += """\n\nMedia will continue to play on other slides."""
        if self._advance:
            gen_content += """\n\nAdvances to next slide """
            if self._synchronous:
                gen_content += """when media ends."""
            else:
                gen_content += """when media starts."""
        return gen_content

    def _target_info(self, target, style):
        """Get information about the specified target.

        target: the target to get info for.
        style: True if the style of the target is desired, False if the
            content is.

        Returns the style or content of the target, as specified by style.

        Overrides the RevSlide._target_info() method because the projector
            target is not the right one to use when other targets are
            missing
        """

        info = self._gen_content()
        try:
            info = self._targets[target]['style'] if style else self._targets[target]['content']
        except KeyError:
            if target == 'projector':
                info = ""
            else:
                if target == 'overview':
                    if 'stage' in self._targets.keys():
                        info = self._targets['stage']['style'] if style else self._targets['stage']['content']
        return info

    def is_media(self):
        """Return True as the slide is a media slide.
        """
        return True

    def media_show_video(self):
        """Return true if there's video content to be displayed.  If the media is
        asynchronous, False is returned.
        """
        if self._media_audio_only:
            return False
        else:
            return self._synchronous

    def media_path(self):
        """Return the path to the media file.
        """
        return self._media_path

    def media_name(self):
        """Return the display name for the media.
        """
        return self._media_name

    def media_start(self):
        """Return the start time of the media.
        """
        return self._start

    def media_end(self):
        """Return the end time of the media.
        """
        return self._end

    def media_volume(self):
        """Return the volume of the media.
        """
        return self._volume

    def media_synchronous(self):
        """Return whether the media is synchronous or not.
        """
        return self._synchronous

    def media_advance(self):
        """Return True if the presentation should automatically go to the next
        slide.
        """
        return self._advance

    def media_loop(self):
        """Return True if the media should loop until manually ended.
        """
        return self._loop

    def always_play(self):
        """Return True if the media play when going backward in the presentation.
        """
        return self._always_play


class RevSlideImage(RevSlideMedia):
    """Image presentation."""
    def __init__(self, filepath, content=None):
        super().__init__(filepath, content)

        # Preview is always generated
        self._targets['preview'] = {'content':
            """<html><head></head><body><img src="{}" /></body></html>""".format(filepath)}


class RevSlideExternal(RevSlide):
    """A file opened with an external program."""
    def __init__(self, filepath, mime_subtype, content=None):
        # Do generic initialisations
        super().__init__(filepath, content)

        # Generic defaults
        self._pres_path = filepath
        self._pres_name = os.path.splitext(os.path.split(filepath)[1])[0]

        # Set up the targets
        if mime_subtype == "pdf":
            self._type = "PDF"
        elif mime_subtype in ('vnd.oasis.opendocument.presentation',
            'vnd.ms-powerpoint',
            'vnd.openxmlformats-officedocument.presentationml.presentation'):
            self._type = "Traditional presentation"

    def _gen_content(self):
        """Generate slide content to use when nothing is supplied.
        """
        return "# {}\n\n({})".format(self._pres_name, self._type)

    def _target_info(self, target, style):
        """Get information about the specified target.

        target: the target to get info for.
        style: True if the style of the target is desired, False if the
            content is.

        Returns the style or content of the target, as specified by style.

        Overrides the RevSlide._target_info() method because the projector
            target is not the right one to use when other targets are
            missing
        """

        info = self._gen_content()
        try:
            info = self._targets[target]['style'] if style else self._targets[target]['content']
        except KeyError:
            if target == 'projector':
                info = ""
            else:
                if target == 'overview':
                    if 'stage' in self._targets.keys():
                        info = self._targets['stage']['style'] if style else self._targets['stage']['content']
        return info

    def is_external(self):
        """Return True if the slide is opened by an external program.
        """
        return True

    def path(self):
        """Return the path to the presentation file.
        """
        return self._pres_path

    def name(self):
        """Return the display name for the presentation.
        """
        return self._pres_name

    def type(self):
        """Return the type of the external file"""
        return self._type

    def advance(self):
        """Return True if the presentation should automatically go to the next
        slide.
        """
        return True


class RevSlideComment(RevSlide):
    """A comment slide type - these are not displayed in any way.
    """
    def slides(self):
        return []
