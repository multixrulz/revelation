#!/usr/bin/python
"""Custom widgets inheriting from Qt widgets.
"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
from PyQt6 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets
from PyQt6.QtWebEngineCore import QWebEngineSettings
#from PyQt5 import QtWebEngineWidgets
# Local / relative imports
from Revelation import config, build_config, mediaplayer

class ClassConverter(object):
    """A class to convert an instance of one class to another class.
    """
    @classmethod
    def create_from(cls, obj, *args, **kwargs):
        obj.__class__ = cls
        obj._instance_init(*args, **kwargs)


class RevWebView(QtWebEngineWidgets.QWebEngineView, ClassConverter):
    """A modified QWebView specifically for Revelation.
    """
    def __init__(self, *args, **kwargs):
        """Initialise the webview.

        The main difference is the lack of scrollbars.
        """
        super().__init__(*args, **kwargs)
        self._instance_init()

    def _instance_init(self):
        """Initialisation to be applied to an instance of QWebView to
        turn it into a RevWebView.
        """
        # Disable scrollbars
        self.settings().setAttribute(QWebEngineSettings.WebAttribute.ShowScrollBars, False)


class RevHtmlDelegate(QtWidgets.QStyledItemDelegate):
    """A delegate responsible for the formatted view in the live and
    preview panes (listviews)
    """
    def __init__(self, *args, **kwargs):
        """Initialise the delegate."""
        # This happens once for the entire program
        super().__init__(*args, **kwargs)
        self.doc = QtGui.QTextDocument(self)

    def paint(self, painter, option, index):
        """Overrides the QStyledItemDelegate method to allow customisations
        to how the HTML is displayed.

        painter, option, index: See Qt doco.
        """
        painter.save()

        if option.widget is None:
            style = QtCore.QApplication.style()
            style.drawControl(QtWidgets.QStyle.CE_ItemViewItem, option, painter)
        else:
            # Passing in the widget ensures that stylesheet information is also passed on
            style = option.widget.style()
            style.drawControl(QtWidgets.QStyle.ControlElement.CE_ItemViewItem, option, painter, option.widget)

        ### Do the painting
        self._update_doc(option, index)
        option.text = "" # Remove the raw HTML from the output

        # Nice quality
        painter.setRenderHints(QtGui.QPainter.RenderHint.SmoothPixmapTransform |
            QtGui.QPainter.RenderHint.Antialiasing)

        # Draw a line under -- has to come before painter.translate.
        painter.fillRect(option.rect.left(), option.rect.bottom(),
            option.rect.width(), 1,
            QtCore.Qt.BrushStyle.SolidPattern)

        # Paint the textdocument
        textRect = style.subElementRect(QtWidgets.QStyle.SubElement.SE_ItemViewItemText, option)
        painter.translate(textRect.topLeft())
        ctx = QtGui.QAbstractTextDocumentLayout.PaintContext()
        self.doc.documentLayout().draw(painter, ctx)

        painter.restore()

    def sizeHint(self, option, index):
        """Override the sizeHint method to ensure that the width of the
        HTML is set to match the width of the listview.

        Returns the necessary size for the rendered HTML.
        """
        # Work out the size
        self._update_doc(option, index)
        size = QtCore.QSize(int(self.doc.textWidth()), int(self.doc.size().height()))
        return size

    def _update_doc(self, option, index):
        """Set image widths appropriately.

        option, index: the same option and index types being passed
            into paint().

        Returns a QTextDocument for rendering.
            """
        self.initStyleOption(option, index) # Grabs the item's data
        self.doc.setTextWidth(option.rect.width())
        html = option.text
        # This is a bit hacky, but I can't see another way to set the image
        # width since Qt's HTML support is so small.
        # Detect an image, and add a width to it
        html = html.replace('<img ',
            '<img width="%i" ' % (option.rect.width() - 2*self.doc.documentMargin()))
        # Another hack, since Qt doesn't really support CSS and I don't want
        # to pollute the library part with code for Qt
        # First, search for '<h1>Error</h1>' in the html. It's not foolproof
        # but it's unlikely to cause problems too often.
        matches = re.findall('<h1>Error</h1>', html)
        # If that was found, add red color to a few select tags that should
        # make the error obvious
        if len(matches) > 0:
            for tag in ('h1', 'h2', 'h3', 'p'):
                html = html.replace('<'+tag+'>', '<'+tag+' style="color: #b00">')
        self.doc.setHtml(html)

rev_icon = QtGui.QIcon(os.path.join(build_config.SYSTEM_ICON_DIR, 'revelation-icon.svg'))

def icon_for_file(file_info):
    """Get an icon based on MIME-type.  Adds a custom icon for the Revelation
    file type.

    file_info: A QFileInfo object to get an icon for.

    Returns a QIcon.
    """
    mime_database = QtCore.QMimeDatabase()
    mimetype = mime_database.mimeTypeForFile(file_info)
    icon_name = mimetype.iconName()
    # Special icon for Revelation text presentations
    if (icon_name.startswith("text") or
        icon_name.startswith("application-x-zerosize")):
        # Special icon for Revelation events and presentations
        if file_info.suffix() == "reve" or file_info.suffix() == "revp":
            icon = rev_icon
        else:
            icon = QtGui.QIcon.fromTheme(icon_name)
    else:
        if not QtGui.QIcon.hasThemeIcon(icon_name):
            icon_name = mimetype.genericIconName()
        icon = QtGui.QIcon.fromTheme(icon_name)
    return icon


class RevFileSystemModel(QtGui.QFileSystemModel, ClassConverter):
    """Adds mimetype and proper icon to QFileSystemModel
    """
    def __init__(self, *args, **kwargs):
        """Initialise the model."""
        super().__init__(*args, **kwargs)
        self._instance_init()

    def _instance_init(self):
        """Initialisation to be applied to an instance of QFileSystemModel
        to turn it into a RevFileSystemModel."""
        pass

    def data(self, index, role):
        """Override the data method to add icons."""
        if role == QtCore.Qt.ItemDataRole.DecorationRole:
            # Detect correct mimetype, get icon for it.
            file_info = self.fileInfo(index)
            icon = icon_for_file(file_info)
            return icon
        else:
            return super().data(index, role)


class RevFileListWidget(QtWidgets.QListWidget, ClassConverter):
    """Customises drag&drop functionality for use in the event/presentation editor.
    """
    def __init__(self, file_to_item, *args, **kwargs):
        """Initialise the list widget"""
        super().__init__(*args, **kwargs)
        self._instance_init()

    def _instance_init(self, file_to_item):
        """Initialisation to be applied to an instance of QListWidget
        to turn it into a RevEventListWidget.

        file_to_item: a function that takes a filepath and returns an item.
        """
        self._file_to_item = file_to_item

    def dragMoveEvent(self, event):
        """Reimplemented to test the data being dragged and decide if it's usable.
        """
        # This should really call an instance method somewhere to
        # determine what's what.  Or should it?
        mime_data = event.mimeData()
        if mime_data.hasFormat('text/uri-list'):
            # From some external widget
            event.accept()
        elif mime_data.hasFormat('application/x-qabstractitemmodeldatalist'):
            # From self
            event.accept()
        else:
            event.ignore()
        super().dragMoveEvent(event)

    def dragEnterEvent(self, event):
        """Reimplemented to decide whether to accept a file dragged over the widget or not."""
        # Required for DnD to work at all
        # Check the start of the path is the presentation directory
        filepath = QtCore.QUrl(event.mimeData().text()).toLocalFile()
        if (filepath.startswith(config.presentation_dir()) and
            os.path.isfile(filepath) and
            os.path.exists(filepath)):
            event.accept()
        else:
            event.ignore()
        super().dragEnterEvent(event)

    def dropEvent(self, event):
        """Reimplemented to add the dropped file to the list."""
        mime_data = event.mimeData()
        if not mime_data.hasFormat('application/x-qabstractitemmodeldatalist'):
            # From some other widget, format the data
            item = self.itemAt(event.position().toPoint())
            if item is None:
                # We must be at the bottom of the list.  Insert at the end.
                row = self.count()
            else:
                self.setCurrentItem(item)
                row = self.currentRow()
            filepath = QtCore.QUrl(event.mimeData().text()).toLocalFile()
            new_item = self._file_to_item(filepath)
            self.insertItem(row, new_item)
        super().dropEvent(event)


class RevMediaWidget(QtWidgets.QWidget, ClassConverter):
    """A widget containing a mediaplayer.  mediaplayer.py is the source of
    the media playing functionality.
    """

    media_started = QtCore.pyqtSignal(str, bool, bool)
    """Arguments are media name, whether video should be shown,
    and if the media slide specifies that it should advance.
    """
    media_ended = QtCore.pyqtSignal(bool)
    """Argument is whether the media ended (as opposed to being stopped).
    """

    def __init__(self, *args, **kwargs):
        """Initialise the widget.
        """
        super().__init__(*args, **kwargs)
        self._instance_init()

    def _instance_init(self):
        """Initialisation to be applied to an instance of QWidget to
        turn it into a RevMediaWidget.
        """
        self._mediaplayer = mediaplayer.RevMediaPlayer(self.get_winid,
            self._media_started_callback, self._media_ended_callback)
        self._media_slide = None

    def get_winid(self):
        return int(self.winId())

    def play(self, media_slide):
        """Plays the media found within a RevSlide.

        media_slide: a RevSlide containing media.
        """
        self._media_slide = media_slide
        self._mediaplayer.play(media_slide)

    def pause_play(self):
        """Pause/play the current media.
        """
        self._mediaplayer.pause_play()

    def beginning(self):
        """Play the current media from the beginning.
        """
        self._mediaplayer.beginning()

    def stop(self):
        """Stop the current media.
        """
        self._mediaplayer.stop()
        self._media_slide = None

    def playing(self):
        """Returns True if media is playing.
        """
        return self._mediaplayer.playing()

    def paused(self):
        """Returns True if media is paused.
        """
        return self._mediaplayer.paused()

    def synchronous(self):
        """Returns True if media is synchronous or there is no media.
        """
        if self._media_slide is not None:
            return self._media_slide.media_synchronous()
        else:
            return True

    # This method exists to work around Qt.  A callback from a different thread
    # (as this one is) cannot modify widgets
    def _media_ended_callback(self, reached_end):
        """Convert a callback method into a Qt signal.

        reached_end: True if media reached the end.
        """
        if reached_end:
            if (self._media_slide.media_advance() and
                self._media_slide.media_synchronous()):
                self.media_ended.emit(True)
            else:
                self.media_ended.emit(False)
        else:
            self.media_ended.emit(False)

    # This method exists to work around Qt.  A callback from a different thread
    # (as this one is) cannot modify widgets
    def _media_started_callback(self):
        """Convert a callback method into a Qt signal.
        """
        if (self._media_slide.media_advance() and not
            self._media_slide.media_synchronous()):
            advance = True
        else:
            advance = False
        self.media_started.emit(self._media_slide.media_name(),
            self._media_slide.media_show_video(),
            advance)


def format(colour, style=''):
    """Return a QTextCharFormat with the given attributes.

    colour: A colour spec that can be used by QtGui.QColor.
    style: A string containing 'bold' or 'italic' or both.
    """
    _colour = QtGui.QColor()
    _colour.setNamedColor(colour)

    _format = QtGui.QTextCharFormat()
    _format.setForeground(_colour)
    if 'bold' in style:
        _format.setFontWeight(QtGui.QFont.Weight.Bold)
    if 'italic' in style:
        _format.setFontItalic(True)
    return _format

blue = '#3754E0'
turquoise = '#279E81'
green = '#2F8A22'
orange = '#C76100'
red = '#9C262F'

slide_def_re = re.compile(r"^(%)\s*([a-zA-Z#]+)\s*(.*)\s*")
slide_def_fmt = [format(blue, 'bold'), format(turquoise, 'bold italic'), format(blue, 'bold')]
slide_def_fmt_error = [format(blue, 'bold'), format(red, 'bold'), format(blue, 'bold')]

new_target_re = re.compile(r"^(@)\s*([a-zA-Z]+)\s*(.*)\s*")
new_target_fmt = [format(green, 'bold'), format(orange, 'bold italic'), format(green, 'bold')]
new_target_fmt_error = [format(green, 'italic'), format(red, 'bold'), format(green, 'italic')]

class RevSyntaxHighlighter(QtGui.QSyntaxHighlighter):
    """A syntax highlighter.
    """

    def highlightBlock(self, text):
        """Apply syntax highlighting to the given block of text.
        """
        # Do other syntax formatting
        # Text blocks appear to be lines
        # Format new slide definitions
        m = slide_def_re.match(text)
        if m is not None:
            # Check for a valid slide type
            if m[2] in ['revelation', 'markdown', 'md', 'media', 'image',
                'colour', '#', 'revp', 'file']:
                fmt = slide_def_fmt
            else:
                fmt = slide_def_fmt_error
            for i in range(len(m.groups())):
                start = m.start(i+1)
                length = m.end(i+1) - m.start(i+1)
                self.setFormat(start, length, fmt[i])

        # Format target definitions
        m = new_target_re.match(text)
        if m is not None:
            # Check for a valid slide type
            if m[2] in ['projector', 'stage', 'mediaplayer', 'preview', 'overview']:
                fmt = new_target_fmt
            else:
                fmt = new_target_fmt_error
            for i in range(len(m.groups())):
                start = m.start(i+1)
                length = m.end(i+1) - m.start(i+1)
                self.setFormat(start, length, fmt[i])
