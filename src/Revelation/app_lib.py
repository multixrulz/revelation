#!/usr/bin/python

"""provides a class to be used by GUI code to run Revelation"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import subprocess
# Local / relative imports
from .presentations import *
from . import config, build_config, webserver
from .exceptions import *


class RevelationApp():
    """RevelationApp knows how to open Revelation files, play media,
    serve the stage view over http, and generally do everything else
    except provide a GUI and run the projector window/monitor.
    """

    def __init__(self):
        """Initialise the class.
        """
        # Initialise this variable so that exception cleanup code will work
        try:
            self._httpserver = webserver.RevHTTPServer()
        except OSError as e:

            raise RevError("Could not start the built-in webserver. Is another Revelation instance running?.\n\nThe error was:\n{}".format(e)) from None
        self.live_slideshow = RevFile().slideshow()
        # Load themes
        self._init_themes()
        # Initialise clock variables - webserver needs them
        self._timer_mode = "none"
        self._timer_end_time = None
        self._show_clock = False
        self._show_elapsed = False
        self._elapsed_start_time = datetime.datetime.today()
        # Load the current (empty) presentation into the webserver
        self._update_webserver()
        self._httpserver.update_overview(self.live_overview())
        self._current_slide = self.live_slideshow.current_slide()
        self._change_slide()
        self.preview_slideshow = None
        self._frozen = False

    ### Open files
    def open(self, filepath):
        """Open a file that Revelation understands.  It is the "live to
        the projector" event/presentation.

        filepath: the path to the file being opened.
        """
        if not os.path.isdir(filepath):
            self.live_slideshow = open_slideshow(filepath)
            self._timer_mode = "none"
            self._timer_end_time = None
            self._show_clock = False
            self._show_elapsed = False
            self._elapsed_start_time = datetime.datetime.today()
            self._httpserver.update_overview(self.live_overview())

    def open_preview(self, filepath):
        """Open a file that Revelation understands.  It is the "preview"
        event or presentation that can be examined without affecting
        the projector.

        filepath: the path to the file being opened.
        """
        if not os.path.isdir(filepath):
            self.preview_slideshow = open_slideshow(filepath)

    ### Open directory
    def open_directory(self, dirpath):
        """Open a directory in the user's preferred file manager.

        dirpath: the path to the directory to open."""
        subprocess.call(['xdg-open', dirpath])

    ### Previews and overview etc
    def live_previews(self):
        """Get a list of slide previews for the "live"
        event/presentation.

        Returns a list of HTML documents as strings.
        """
        return [s.html('preview') for s in self.live_slideshow.slides]

    def preview_previews(self):
        """Get a list of slide previews for the "preview"
        event/presentation.

        Returns a list of HTML documents as strings.
        """
        return [s.html('preview') for s in self.preview_slideshow.slides]

    def live_overview(self):
        """Get the overview for the live event/presentation.

        Returns a HTML document as a sting.
        """
        return self.live_slideshow.overview()

    ### Navigation in presentation/event
    def current_slide_number(self):
        """Return the slide number of the current live event/presentation.
        """
        return self.live_slideshow.current_slide_number()

    def current_slide(self):
        """Return the RevSlide of the current life event/presentation.
        """
        return self.live_slideshow.current_slide()

    def first_slide(self):
        """Go to the first slide of the live event/presentation."""
        self.live_slideshow.go_first_slide()
        self._change_slide()

    def next_slide(self):
        """Go to the next slide of the live event/presentation.

        Returns true if the slide was advanced, false if already on the
        last slide.
        """
        slide_changed = self.live_slideshow.go_next_slide()
        if slide_changed:
            self._change_slide()
        return slide_changed

    def previous_slide(self):
        """Go to the previous slide of the live event/presentation."""
        self.live_slideshow.go_previous_slide()
        self._change_slide()

    def go_slide(self, slide_number):
        """Go to the specified slide of the live event/presentation.

        slide_number: the number of the slide to go to.
        """
        self.live_slideshow.go_slide(slide_number)
        self._change_slide()

    def set_freeze_state(self, frozen):
        """Set the "frozen" state of Revelation.  When Revelation is
        "frozen", the projector is not updated when the current slide is
        changed.

        frozen: a boolean.
        """
        self._frozen = frozen

    def theme_changed(self):
        """Notify RevelationApp that the theme has been changed."""
        self._update_webserver()

    def _change_slide(self):
        """Updates the webserver and the current slide."""
        if self._current_slide != self.live_slideshow.current_slide():
            if not self._frozen:
                # Swap over to new slide
                self._current_slide = self.live_slideshow.current_slide()
                # Change the timer if necessary
                if self._current_slide._timer_mode is not None:
                    self._timer_mode = self._current_slide._timer_mode
                    self._timer_end_time = None # Default value for no timer
                    # Set the end time for the timer if in timer or alarm modes
                    if self._timer_mode == "timer":
                        # Add duration to current time
                        self._timer_end_time = datetime.datetime.today() + self._current_slide._timer_duration
                    elif self._timer_mode == "alarm":
                        # Use alarm time as given
                        self._timer_end_time = self._current_slide._timer_alarm
                if self._current_slide._show_clock is not None:
                    if (self._show_clock == False
                        and self._current_slide._show_clock == True):
                        self._elapsed_start_time = datetime.datetime.today()
                    self._show_clock = self._current_slide._show_clock
                if self._current_slide._show_elapsed is not None:
                    self._show_elapsed = self._current_slide._show_elapsed
                # Update webserver
                self._update_webserver()

    def _update_webserver(self):
        """Sends current slide to the webserver"""
        # Projector
        for (theme_path, url_path, theme_name) in self._themes:
            if theme_path == config.current_theme_path():
                theme = url_path
                break
        current_slide = self.live_slideshow.current_slide()
        self._httpserver.update_slide_content(
            theme,
            current_slide.html('projector'),
            current_slide.html('stage'),
            current_slide.style('projector'),
            current_slide.style('stage'),
            self._timer_mode,
            self._timer_end_time,
            self._show_clock,
            self._show_elapsed,
            self._elapsed_start_time,
            )

    ### Theme lists
    def themes(self):
        return self._themes

    def _init_themes(self):
        """Return a list of (theme_path, url_path, theme_name) tuples for themes
            found on the system.
        """
        # Add user themes
        self._themes = self.__theme_list(config.theme_dir(), "/user/")
        # Add system themes
        self._themes.extend(self.__theme_list(build_config.SYSTEM_THEME_DIR, "/system/", "(System)"))

    def __theme_list(self, directory, path_prefix, suffix=None):
        """Creates a list of themes (directories) found in the given
            directory.

            directory: the directory to search.
            path_prefix: a prefix to be added to the path
            suffix: string to be added to the end of a theme name.
        """
        dirlist = os.listdir(directory)
        dirlist.sort()
        themes = []
        for d in dirlist:
            path = (os.path.join(directory, d))
            if os.path.isdir(path):
                themes.append((path, os.path.join(path_prefix, d),
                    "{} {}".format(d, suffix) if suffix is not None else d))
        return themes

    ### Cleanup
    def quit(self):
        """Stop the webserver for a clean application exit."""
        self._httpserver.exit()
