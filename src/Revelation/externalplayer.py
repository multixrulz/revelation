#!/usr/bin/python
"""Manage the playing of audio and video files."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import subprocess
import time
import threading
import tempfile
import socket
import os

class RevExternalPlayer():
    """A wrapper around various programs that display non-revelation content."""
    def __init__(self, playback_started_callback,
        playback_ended_callback, *args, **kwargs):
        """Initialise the player.

        playback_started_callback: the function to call when playback starts.
        playback_ended_callback: the function to call when playback ends.
        """
        super().__init__(*args, **kwargs)
        self._playback_ended_callback = playback_ended_callback
        self._playback_started_callback = playback_started_callback
        self._process = None

    def play(self, external_slide):
        """Play an external file.

        external_slide: a RevSlide containing the external file.
        """

        # Only allow one instance of mpv.
        self.exit()
        if external_slide.type() == "PDF":
            # do pdf viewer
            commandline = ['pdfpc', '-w', 'none', external_slide.path()]
        elif external_slide.type() == "Traditional presentation":
            # do impress
            commandline = ['libreoffice', '--nologo', '--show',external_slide.path()]
        try:
            self._process = subprocess.Popen(commandline)
            self._wait_thread = threading.Thread(target=self._wait_process)
            self._wait_thread.start()
            self._playback_started_callback()
        except Exception as e:
            print("revelation: external program call failed with exception:\n", e)

    def _wait_process(self):
        """Wait for mpv to exit.
        """
        self._process.wait()
        self._playback_ended_callback()

    def exit(self):
        """Stop playing the external file.
        """
        if self._process is not None: # It's initialised to None
            self._process.kill()
            # Wait for it to end before continuing, so we don't make a
            # mess of things (especially callbacks)
            self._process.wait()
