#!/usr/bin/python

"""The Revelation file editor"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6 import uic
import os
from .qt_extensions import *
#from . import app_lib
#from . import config_qt5
#from . import version
from . import build_config
from . import config
from . import presentations
from . import mime


class RevFileEditor(QtWidgets.QDialog):
    """The Revelation composite (.reve and .revp) editor window."""
    def __init__(self, filename, *args, **kwargs):
        """Initialise the window.

        filename: the composite file to load into the window.
        """
        super().__init__(*args, **kwargs)
        self._filename = filename
        self._init_gui()
        self._convert_widgets()
        self._init_presentation_browser()
        self._load_revfile(filename)
        self.setWindowTitle("File Editor: {}".format(filename))
        self.show()

    def _init_gui(self):
        """Load the QtCreator file and do GUI customisations."""
        uic.loadUi(os.path.join(build_config.UI_DIR, "RevelationFileEditor.ui"), self)
        delegate = RevHtmlDelegate()
        self.listWidget_file.setItemDelegate(delegate)
        # Make listviews less jumpy
        self.listWidget_file.verticalScrollBar().setPageStep(2)
        self.listWidget_file.verticalScrollBar().setSingleStep(10)
        self.treeView_presentation.verticalScrollBar().setPageStep(2)
        self.treeView_presentation.verticalScrollBar().setSingleStep(10)
        # Connect to the revelation file selection to enable/disable file buttons
        self.listWidget_file.itemSelectionChanged.connect(self._set_buttons)
        self._set_buttons()

    def _convert_widgets(self):
        """Convert from standard Qt widgets to Revelation-specific ones."""
        RevFileListWidget.create_from(self.listWidget_file, self._file_to_item)

    def _init_presentation_browser(self):
        """Initialise the file treeview."""
        # Set a model and populate it
        fsmodel = RevFileSystemModel()
        fsmodel.setReadOnly(True)
        fsmodel.setResolveSymlinks(True)
        fsmodel.setNameFilterDisables(False)
        # Set up the root path depending on whether we're editing an
        # event or a revp presentation.
        event_root = os.path.realpath(config.event_dir())
        file_path = os.path.realpath(self._filename)
        if os.path.commonpath([event_root, file_path]) == event_root: # event
            self._presentation_root = config.presentation_dir()
        else: # revp
            self._presentation_root = os.path.dirname(self._filename)
        rootIndex = fsmodel.setRootPath(self._presentation_root)
        self.treeView_presentation.setModel(fsmodel)
        self.treeView_presentation.setRootIndex(rootIndex)
        headerview = self.treeView_presentation.header()
        headerview.hideSection(1)
        headerview.hideSection(2)
        headerview.hideSection(3)
        self._presentation_fsmodel = fsmodel

    def _set_buttons(self):
        """Update the remove/edit buttons based on the slide treeview's selection."""
        items = self.listWidget_file.selectedItems()
        has_selection = len(items) > 0
        self.pb_remove.setEnabled(has_selection)
        self.pb_edit.setEnabled(has_selection)

    def _presentation_filter_changed(self, new_string):
        """Filter the presentation treeview."""
        # If there's something in the filter string, expand all
        # items.  If it's empty, collapse all items.
        # Do the filtering
        filter = "*{}*".format(new_string)
        self._presentation_fsmodel.setNameFilters([filter,])

    def _presentation_activated(self, index):
        """Move the activated presentation to the file."""
        # Get then file path, relative to the presentations directory
        file_info = self.treeView_presentation.model().fileInfo(index)
        if file_info.isFile() and file_info.isReadable():
            file_path = file_info.canonicalFilePath()
            row = self._row_for_insert()
            item = self._file_to_item(file_path)
            self.listWidget_file.insertItem(row, item)
            self.listWidget_file.setCurrentRow(row)

    def _load_revfile(self, filepath):
        """Load the Revelation file.

        filepath: The path of the file to open"""
        self._filepath = filepath
        try:
            raw_slides = presentations.parse_file(filepath)
        except presentations.RevError as e:
            # Show the error - this is somewhat duplicated from
            # presentations.RevSlideError()
            slide_content = str(e)
            errortext = '# Error\n\n{}\n\n{}'.format(filepath, slide_content)
            raw_slide = presentations.parse_slide(['% error {}'.format(filepath), slide_content])
            raw_slides = [raw_slide,]
            # Disable saving
            self.pb_save.setEnabled(False)

        for rs in raw_slides:
            self.listWidget_file.addItem(self._raw_slide_to_item(rs))

    format_map = { # The tuple is: (arg beside type, show content)
        'markdown': (True, True),
        'md': (True, True),
        'media': (False, False),
        'image': (False, False),
        'file': (False, False),
        '#': (True, True),
        'revp': (False, False),
        'error': (True, True),
        '': (True, True)}

    def _raw_slide_to_item(self, raw_slide):
        """Convert a raw slide to an item.

        raw_slide: the raw slide that we want to add to a QListWidget
            (a tuple: (slide_type, slide_argument, slide_content))

        Returns a QListWidgetItem.
        """
        item_text = self._format_slide_text(raw_slide)
        item = QtWidgets.QListWidgetItem(item_text)
        item.setData(QtCore.Qt.ItemDataRole.UserRole, raw_slide)
        return item

    def _format_slide_text(self, raw_slide):
        """Format slide text.

        raw_slide: the raw slide (a tuple: (slide_type, slide_argument, slide_content))

        Returns HTML-formatted text for display in the list widget.
        """
        (slide_type, slide_arg, slide_content) = raw_slide
        # Add to the revelation file list
        try:
            (arg_beside_type, show_content) = self.format_map[slide_type]
        except KeyError:
            (arg_beside_type, show_content) = (False, True)
        heading = "%s %s" % (slide_type, slide_arg) if arg_beside_type else slide_type
        if show_content:
            content = slide_content['projector']['content'] if len(slide_content) > 0 else ''
        else:
            content = slide_arg
        item_text = "<h3>%s</h3><p>%s</p>" % (heading, content)
        # Make comments grey
        if slide_type == '#':
            for tag in ('h3', 'p'):
                item_text = item_text.replace('<'+tag+'>', '<'+tag+' style="color: gray">')
        # Fix line breaks with usable HTML
        item_text = item_text.replace('\n\n', '\n<br>')
        return item_text

    def _file_to_item(self, file_path):
        """Create an item for the given file path

        file_path: the path that we want to add to a QListWidget.

        Returns a QListWidgetItem
        """
        mimetype = mime.mimetype(file_path)
        relative_path = os.path.relpath(file_path, self._presentation_root)
        raw_slide = None
        # Text mimetypes
        if mimetype.media == 'text':
            extension = os.path.splitext(file_path)[1]
            if extension == '.revp':
                raw_slide = ['file', relative_path, {}]
        # Images - only accept HTML-supported image types
        elif mimetype.media == 'image':
            if mimetype.subtype in ('jpeg', 'gif', 'png', 'svg'):
                raw_slide = ['file', relative_path, {}]
        # Video & Audio
        elif mimetype.media == 'video' or mimetype.media == 'audio':
            raw_slide = ['file', relative_path, {}]
        elif mimetype.media == 'application':
            if mimetype.subtype in ('pdf',
                'vnd.oasis.opendocument.presentation',
                'vnd.ms-powerpoint',
                'vnd.openxmlformats-officedocument.presentationml.presentation'):
                raw_slide = ['file', relative_path, {}]
        if raw_slide:
            return self._raw_slide_to_item(raw_slide)

    def _row_for_insert(self):
        """Get the row number to insert a new item at when not using
        drag and drop.

        Returns the row number to use for the list insertion.
        """
        # Return the row after the current selection, or the end of the
        # list if there is no selection.
        row = self.listWidget_file.count()
        items = self.listWidget_file.selectedItems()
        if len(items) > 0:
            row = self.listWidget_file.currentRow() + 1
        return row

    def _add_clicked(self):
        """Callback for the "add" pushbutton.  Adds an empty markdown
            slide to the list
        """
        raw_slide = ['markdown', '', {}]
        item = self._raw_slide_to_item(raw_slide)
        row = self._row_for_insert()
        self.listWidget_file.insertItem(row, item)
        self.listWidget_file.setCurrentRow(row)

    def _edit_clicked(self):
        """Callback for the "edit" pushbutton.  Open the raw file editor
            with the specified text.
        """
        item = self.listWidget_file.selectedItems()[0]
        raw_slide = item.data(QtCore.Qt.ItemDataRole.UserRole)
        text = presentations.slide_text(raw_slide)
        editor = RevRawFileEditor(text)
        editor.exec()
        if editor.result() == QtWidgets.QDialog.DialogCode.Accepted:
            lines = editor.text.splitlines()
            # First line needs to be a slide definition.  If not, assume markdown.
            if presentations.is_new_slide(lines[0]):
                slide_def = lines[0]
                slide_content = lines[1:]
            else:
                slide_def = '% markdown'
                slide_content = lines
            # Do not treat other lines as slide definitions - escape any % found
            # at the beginning of a line
            for i, line in enumerate(slide_content):
                if line.startswith('%'):
                    slide_content[i] = '%{}'.format(line)
            raw_slide = presentations.parse_slide((slide_def, slide_content))
            item_text = self._format_slide_text(raw_slide)
            item.setData(QtCore.Qt.ItemDataRole.DisplayRole, item_text)
            item.setData(QtCore.Qt.ItemDataRole.UserRole, raw_slide)

    def _remove_clicked(self):
        """Remove a presentation from the Revelation file."""
        items = self.listWidget_file.selectedItems()
        for item in items:
            row = self.listWidget_file.row(item)
            self.listWidget_file.takeItem(row)
            del item

    def _save(self):
        """Save the Revelation file."""
        # Go through the file listview and write out everything that's found there
        raw_slides = [self.listWidget_file.item(i).data(QtCore.Qt.ItemDataRole.UserRole)
            for i in range(self.listWidget_file.count())]
        slide_text = [presentations.slide_text(rs) for rs in raw_slides]
        with open(self._filename, 'w') as f:
            f.write("% revelation\n")
            f.write("\n".join(slide_text))

    def _cancel_clicked(self):
        """Close the window."""
        self.reject()

    def _save_clicked(self):
        """Save the edits and close the window."""
        self._save()
        self.accept()

class RevRawFileEditor(QtWidgets.QDialog):
    """The Revelation raw file editor window."""
    def __init__(self, text, *args, **kwargs):
        """Initialise the window.

        text: the text to load into the window.
        """
        super().__init__(*args, **kwargs)
        uic.loadUi(os.path.join(build_config.UI_DIR, "RevelationRawFileEditor.ui"), self)
        # Need to keep a reference to the highlighter or it'll be garbage # collected and stop working
        self.highlight = RevSyntaxHighlighter(self.plainTextEdit.document())
        self.show()
        self.plainTextEdit.setPlainText(text)

    def _cancel_clicked(self):
        """Close the window."""
        self.reject()

    def _save_clicked(self):
        """Accept the edits and close the window."""
        self.text = self.plainTextEdit.toPlainText()
        self.accept()
