#!/usr/bin/python
"""Manage the configuration via a Qt5 GUI."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import uic
# Local / relative imports
from .config import *

class ConfigWindow(QtWidgets.QDialog):
    """The Revelation configuration window.
    """
    def __init__(self, *args, **kwargs):
        """Initialise the window.
        """
        super().__init__(*args, **kwargs)
        self._init_gui()
        self.show()

    def _init_gui(self):
        """Load the QtCreator file and set initial values from the
        configuration.
        """
        uic.loadUi(os.path.join(build_config.UI_DIR, "RevelationConfigWindow.ui"), self)
        self.lineEdit_events.setText(event_dir())
        self.lineEdit_presentations.setText(presentation_dir())
        self.lineEdit_themes.setText(theme_dir())

    def _event_button_clicked(self):
        """Select the event directory using a filepicker.
        """
        selected_dir = self.lineEdit_events.text()
        new_dir = self._select_file(selected_dir)
        self.lineEdit_events.setText(new_dir)

    def _presentation_button_clicked(self):
        """Select the presentation directory using a filepicker.
        """
        selected_dir = self.lineEdit_presentations.text()
        new_dir = self._select_file(selected_dir)
        self.lineEdit_presentations.setText(new_dir)

    def _theme_button_clicked(self):
        """Select the theme directory using a filepicker.
        """
        selected_dir = self.lineEdit_themes.text()
        new_dir = self._select_file(selected_dir)
        self.lineEdit_themes.setText(new_dir)

    def _select_file(self, current_selection):
        """Show a filepicker dialog.

        current_selection: the directory that is currently configured.
        """
        filedialog = QtWidgets.QFileDialog()
        filedialog.setFileMode(QtWidgets.QFileDialog.Directory)
        filedialog.setDirectory(os.path.dirname(current_selection))
        filedialog.selectFile(os.path.split(current_selection)[1])
        if (filedialog.exec()):
            # There will be only one selection
            return filedialog.selectedFiles()[0]
        else:
            return current_selection

def open_config_dialog():
    """Open the configuration window.  It can be opened from multiple
    places in code (specifically, the main window and at application
    startup.
    """
    config_window = ConfigWindow()
    result = config_window.exec()
    if result == QtWidgets.QDialog.Accepted:
        # Save the changes
        set_event_dir(config_window.lineEdit_events.text())
        set_presentation_dir(config_window.lineEdit_presentations.text())
        set_theme_dir(config_window.lineEdit_themes.text())
        save()
    elif result == QtWidgets.QDialog.Rejected:
        # Don't save the changes
        pass

def check_config():
    """Check that the configuration file exists and is sane, and fix the
    situation if it is not.
    """
    while not config_exists():
        if _create_config():
            # User has asked to exit
            exit()
    while not config_dirs_exist():
        if _create_config_dirs():
            # User has asked to exit
            exit()


def _create_config():
    """Create a configuration file interactively.
    """
    # If there's no config file, suggest to the user that we create a default
    # configuration, or allow them to select existing directories.
    primary_text = "No configuration found."
    secondary_text = "It looks like this is the first time you've run Revelation, because there's no Revelation configuration file.  Do you want to go ahead with Revelations's default settings, or configure Revelation yourself?"
    msgbox = QtWidgets.QMessageBox()
    msgbox.setWindowTitle("No configuration found")
    msgbox.setIcon(QtWidgets.QMessageBox.Question)
    msgbox.setText("<h1>{}</h1>{}".format(primary_text, secondary_text))
    defaults = msgbox.addButton("Use defaults", QtWidgets.QMessageBox.AcceptRole)
    configure = msgbox.addButton("Configure myself", QtWidgets.QMessageBox.RejectRole)
    close = msgbox.addButton(QtWidgets.QMessageBox.Close)
    msgbox.setDefaultButton(defaults)
    msgbox.exec()
    if msgbox.clickedButton() == defaults:
        save()
        # Create the default directories
        os.makedirs(event_dir(), exist_ok=True)
        os.makedirs(presentation_dir(), exist_ok=True)
        os.makedirs(theme_dir(), exist_ok=True)
        # Also symlink system-wide themes into this directory
    elif msgbox.clickedButton() == configure:
        save()
        open_config_dialog()
    # Return True if the close button was clicked (app should exit)
    return msgbox.clickedButton() == close

def _create_config_dirs():
    """Create the configuration directories interactively.
    """
    # If the three configuration directories don't exist, suggest to the
    # user that we create them or modify the config.
    event_dir_missing = not os.path.isdir(event_dir())
    presentation_dir_missing = not os.path.isdir(presentation_dir())
    theme_dir_missing = not os.path.isdir(theme_dir())
    primary_text = "Data directories not found."
    secondary_text = ["<p>The following directories don't exist.</p>",
        "<ul>"]
    if event_dir_missing:
        secondary_text.append("<li>Events directory.</li>")
    if presentation_dir_missing:
        secondary_text.append("<li>Presentations directory.</li>")
    if theme_dir_missing:
        secondary_text.append("<li>Themes directory.</li>")
    secondary_text.extend(["</ul>",
        "Revelation cannot run without them.",
        "Do you want Revelation to create these directories,",
        "or do you want to configure these directory locations now?</p>"])
    secondary_text = " ".join(secondary_text)
    msgbox = QtWidgets.QMessageBox()
    msgbox.setWindowTitle("No configuration found")
    msgbox.setIcon(QtWidgets.QMessageBox.Question)
    msgbox.setText("<h1>{}</h1>{}".format(primary_text, secondary_text))
    create = msgbox.addButton("Create", QtWidgets.QMessageBox.AcceptRole)
    configure = msgbox.addButton("Configure", QtWidgets.QMessageBox.RejectRole)
    close = msgbox.addButton(QtWidgets.QMessageBox.Close)
    msgbox.setDefaultButton(create)
    msgbox.exec()
    if msgbox.clickedButton() == create:
        # Create the configured directories
        if event_dir_missing:
            os.makedirs(event_dir(), exist_ok=True)
        if presentation_dir_missing:
            os.makedirs(presentation_dir(), exist_ok=True)
        if theme_dir_missing:
            os.makedirs(theme_dir(), exist_ok=True)
            # Also symlink system-wide themes into this directory
    elif msgbox.clickedButton() == configure:
        open_config_dialog()
    # Return True if the close button was clicked (app should exit)
    return msgbox.clickedButton() == close
