#!/usr/bin/python

"""Configuration functionality.

Do not create an instance of the _Config class.  Call the functions in this module instead."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import configparser
import os
import xdg.BaseDirectory
# Local / relative imports
from . import build_config

class _Config():
    """Manage the configuration file.
    """
    # Application defaults
    app_defaults = {'Revelation': { # [Revelation] section of config file
        'event_dir': "~/Revelation/events",
        'presentation_dir': "~/Revelation/presentations",
        'theme_dir': "~/Revelation/themes",
        'current_theme': os.path.join(build_config.SYSTEM_THEME_DIR, 'traditional')}
        }

    ### Holds and allows editing of configuration information.
    def __init__(self):
        """Read the configuration file.
        """
        # Path to config file
        self._config_filename = os.path.join(
            xdg.BaseDirectory.save_config_path('Revelation'),
            'Revelation.conf')

        # Create a configparser object, initialise it with defaults,
        # and read in the config file
        self._cp = configparser.ConfigParser()
        self._cp.read_dict(self.app_defaults)
        self._cp.read(self._config_filename)

    def save(self):
        """Save the configuration file.
        """
        with open(self._config_filename, 'w') as configfile:
            self._cp.write(configfile)

_config = _Config()

def event_dir():
    """Returns the root directory for events.
    """
    return os.path.expanduser(_config._cp.get('Revelation', 'event_dir'))

def set_event_dir(path):
    """Sets the root directory for events.

    path: the desired root directory for events.
    """
    return _config._cp.set('Revelation', 'event_dir', path)

def presentation_dir():
    """Returns the root directory for presentations.
    """
    return os.path.expanduser(_config._cp.get('Revelation', 'presentation_dir'))

def set_presentation_dir(path):
    """Sets the root directory for presentations.

    path: the desired root directory for presentations.
    """
    return _config._cp.set('Revelation', 'presentation_dir', path)

def theme_dir():
    """Returns the root directory for themes.
    """
    return os.path.expanduser(_config._cp.get('Revelation', 'theme_dir'))

def set_theme_dir(path):
    """Sets the root directory for themes.

    path: the desired root directory for themes.
    """
    return _config._cp.set('Revelation', 'theme_dir', path)

def current_theme_path():
    """Returns the path of the currently selected theme.

    """
    # Does the theme directory exist? If not, fall back to the default theme
    theme_path = _config._cp.get('Revelation', 'current_theme')
    if not os.path.isdir(theme_path):
        reset_theme_to_default()
    # If the default theme doesn't exist, just let the badness happen
    return _config._cp.get('Revelation', 'current_theme')

def set_current_theme_path(path):
    """Sets the path of the currently selected theme.

    path: the path of the desired current theme.
    """
    return _config._cp.set('Revelation', 'current_theme', path)

def reset_theme_to_default():
    """Set the path of the currently selected theme to the system default.
    """
    path = set_current_theme_path(_Config.app_defaults['Revelation']['current_theme'])
    save()
    return path

def save():
    """Save the configuration file.
    """
    _config.save()

def config_exists():
    """Return true if there is a configuration file, and false if there isn't.
    """
    return os.path.isfile(_config._config_filename)

def config_dirs_exist():
    """Return true if the configured directories exist.
    """
    event_dir_exists = os.path.isdir(event_dir())
    presentation_dir_exists = os.path.isdir(presentation_dir())
    theme_dir_exists = os.path.isdir(theme_dir())
    return event_dir_exists and presentation_dir_exists and theme_dir_exists

