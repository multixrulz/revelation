#!/bin/bash

echo "Setting correct permissions on ui files"
chmod -R 644 src/ui/*.ui
echo "Setting correct permissions on other files"
find src/other -type f ! -perm 0644 -print -execdir chmod 0644 {} +
find src/other -type d ! -perm 0755 -print -execdir chmod 0755 {} +
python setup.py sdist
